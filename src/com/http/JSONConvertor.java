package com.http;


import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

public class JSONConvertor {

    public static JsonObject convert(String data){
        JsonParser parser = new JsonParser();
        JsonElement rootNode = parser.parse(data);
        JsonObject object = null;
        if(rootNode.isJsonObject()){
            object = rootNode.getAsJsonObject();
        }
        return  object;
    }
}

package com.http;

import com.constants.ProjectConstants;
import com.models.StatusFeeder;


import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class MainHttpCall {
   private URL url = null;
   private HttpURLConnection connection = null;
   private BufferedReader reader = null;
    private StatusFeeder feeder;

   public  MainHttpCall() {
    feeder = new StatusFeeder();
   }
   public StatusFeeder GET(String address, String token) {
       StringBuilder response = new StringBuilder();
        try{
            url = new URL(address);
            connection = (HttpURLConnection)url.openConnection();
            connection.setRequestMethod("GET");
            connection.setRequestProperty("content-type","Application/json");
            connection.setRequestProperty("Authorization", "Bearer "+token);
            connection.setDoInput(true);
            connection.setDoInput(true);
            //connection.connect();

            int responseCode = connection.getResponseCode();
            reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            if(responseCode==200){
                String line = null;

                while ((line =reader.readLine()) != null){
                    response.append(line);
                }
            }else {
                response.append("");
            }

           return  response(responseCode, response);
        }catch (Exception e){
            e.printStackTrace();
            return  null;
        }
   }

   public StatusFeeder POST(String address, String body){
       try {
           url = new URL(address);
           connection = (HttpURLConnection)url.openConnection();
           connection.setRequestMethod("POST");
           connection.setRequestProperty("User-Agent", ProjectConstants.USER_AGENT);
           connection.setRequestProperty("Accept-Language", "en-US");
           String urlParamenters = body;
           connection.setDoOutput(true);
           connection.setDoInput(true);
           DataOutputStream wr = new DataOutputStream(connection.getOutputStream());
           wr.writeBytes(urlParamenters);
           wr.flush();
           wr.close();

           int responseCode = connection.getResponseCode();
           StringBuilder response = new StringBuilder();
           if(responseCode==200){
               BufferedReader  reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
               String line = null;
               while ((line =reader.readLine()) != null){
                   response.append(line);
               }
           }else {
               response.append("");
           }

        return  this.response(responseCode, response);
       }catch (Exception e){
        return null;
       }
   }

   public void SHOW(String address, int id){

   }


   public StatusFeeder response(int responseCode, StringBuilder data){
       if(responseCode==200){
           feeder.setStatus(true);
           feeder.setData(data.toString());
           feeder.setCode(responseCode);
       }else if(responseCode==403){
           feeder.setStatus(false);
           feeder.setData("Unauthorized users");
           feeder.setCode(responseCode);
       }
       return feeder;
   }
}

package com.properties;

import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;

public class PropertWriter {
    private static Properties prop = null;
    private static FileOutputStream output = null;

    public PropertWriter() {
    }

    public static void create(String key, String value) {

        prop = new Properties();
        try {

            output = new FileOutputStream("config.properties");

            // set the properties value
            prop.setProperty(key, value);

            // save properties to project root folder
            prop.store(output, null);

        } catch (IOException io) {
            io.printStackTrace();
        } finally {
            if (output != null) {
                try {
                    output.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

        }
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.Timer;

/**
 *
 * @author MESERET
 */
public class loaderFrame{
   private JDialog dialog;
   private ImagePanel panel;
    public loaderFrame() {
        panel = new ImagePanel();
       dialog = new JDialog();
       dialog.setUndecorated(true);
       dialog.setBackground(new Color(0,0,0,1));
       dialog.setSize(panel.getImage().getWidth()+10, panel.getImage().getHeight()+30);
       int x = (Toolkit.getDefaultToolkit().getScreenSize().width/2-dialog.getWidth()/2);
       int y = (Toolkit.getDefaultToolkit().getScreenSize().height/2-dialog.getHeight()/2);
       dialog.setModal(true);
       dialog.setLocation(x, y);
       dialog.add(panel);
       dialog.setVisible(true);
    }
    class ImagePanel extends JPanel{
      private BufferedImage image;
      private int i;
      private Timer timer;
   
        public ImagePanel() {
          try {
              image = ImageIO.read(getClass().getResource("/images/loading.png"));
          } catch (IOException ex) {
          }
          timer=new Timer(5, new ActionListener() {

              @Override
              public void actionPerformed(ActionEvent e) {
                  i+=10;
                  repaint();
              }
          });
          timer.start();
        }
       public BufferedImage getImage(){
        return image;
       }
      @Override
        public void paintComponent(Graphics g){
            Graphics2D g2 =(Graphics2D)g;
            AffineTransform transform = AffineTransform.getTranslateInstance(0, 0);
            transform.rotate(Math.toRadians(i),image.getWidth()/2,image.getHeight()/2);
            g2.drawImage(image, transform, null);
            g2.setColor(Color.GREEN.darker());
            g2.setFont(new Font("arial",Font.BOLD,20));
            g2.drawString("Loading...",10, image.getHeight()+25);
        }
    
    }
    
}

package com;

import com.loginComponents.LoginFrame;
import org.opencv.core.Core;

import javax.swing.*;
import java.awt.*;

public class MainTester {
    public static  void main(String[] args){
        //System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
            UIManager.getDefaults().put("TabbedPane.contentBorderInsets", new Insets(0, 0, 0, 0));
            UIManager.getDefaults().put("TabbedPane.tabAreaInsets", new Insets(0, 0, 0, 0));
            UIManager.getDefaults().put("TabbedPane.tabsOverlapBorder", true);
            UIManager.put("TabbedPane.borderColor", Color.RED);
        } catch (Exception e) {
        }
        LoginFrame frame = new LoginFrame("FreePark desktop V1.0");
        frame.setVisible(true);    }
}

package com.loginComponents;

import com.cashiersDashboard.mains.CashiersDashboardFrame;
import com.constants.ProjectConstants;
import com.google.gson.*;
import com.http.JSONConvertor;
import com.http.MainHttpCall;
import com.models.Cashiers;
import com.models.StatusFeeder;
import com.models.User;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class LoginPanel extends JPanel {
    SpringLayout mainLayout;
    JLabel loginInfo, emailLabel,passwordLabel, errorShower;
    JTextField emailAddress;
    JPasswordField passwordField;
    JButton loginBtn;
    public LoginPanel() {
        setBackground(Color.decode("#242424"));
         setBorder(new EmptyBorder(10, 50, 10, 100));

         mainLayout = new SpringLayout();
         setLayout(mainLayout);

         // adding login text
         loginInfo = new JLabel("Login to FreePark");
         loginInfo.setForeground(Color.WHITE);
         loginInfo.setFont(new Font("arial", Font.PLAIN, 35));
         add(loginInfo);

         //adding email label
        emailLabel = new JLabel("Email address");
        emailLabel.setForeground(loginInfo.getForeground());
        emailLabel.setFont(new Font("arial",Font.PLAIN,20));
        add(emailLabel);

        //adding email input
        emailAddress = new JTextField();
        emailAddress.setPreferredSize(new Dimension(250,30));
        add(emailAddress);

        // adding password component
        passwordLabel = new JLabel("Password");
        passwordLabel.setForeground(loginInfo.getForeground());
        passwordLabel.setFont(emailLabel.getFont());
        add(passwordLabel);

        passwordField = new JPasswordField();
        passwordField.setPreferredSize(emailAddress.getPreferredSize());
        add(passwordField);

        loginBtn = new JButton("Login");
        loginBtn.setPreferredSize(emailAddress.getPreferredSize());
        add(loginBtn);

        errorShower = new JLabel();
        errorShower.setForeground(Color.RED);
        errorShower.setFont(emailLabel.getFont());
        add(errorShower);

        mainLayout.putConstraint(SpringLayout.WEST, loginInfo,300,SpringLayout.WEST, this);
        mainLayout.putConstraint(SpringLayout.NORTH, loginInfo,50, SpringLayout.NORTH,this );

        mainLayout.putConstraint(SpringLayout.WEST,emailLabel,200,SpringLayout.WEST, this);
        mainLayout.putConstraint(SpringLayout.NORTH,emailLabel,70,SpringLayout.NORTH, loginInfo);

        mainLayout.putConstraint(SpringLayout.WEST, emailAddress, 200, SpringLayout.WEST, emailLabel);
        mainLayout.putConstraint(SpringLayout.NORTH, emailAddress, 65, SpringLayout.NORTH, loginInfo);

        mainLayout.putConstraint(SpringLayout.WEST, passwordLabel, 200, SpringLayout.WEST, this);
        mainLayout.putConstraint(SpringLayout.NORTH, passwordLabel, 65, SpringLayout.NORTH, emailLabel);

        mainLayout.putConstraint(SpringLayout.WEST, passwordField, 200, SpringLayout.WEST, passwordLabel);
        mainLayout.putConstraint(SpringLayout.NORTH, passwordField, 65, SpringLayout.NORTH, emailAddress);

        mainLayout.putConstraint(SpringLayout.WEST, loginBtn, 200, SpringLayout.WEST, passwordLabel);
        mainLayout.putConstraint(SpringLayout.NORTH, loginBtn, 65, SpringLayout.NORTH, passwordField);

        mainLayout.putConstraint(SpringLayout.WEST, errorShower,200, SpringLayout.WEST, this);
        mainLayout.putConstraint(SpringLayout.NORTH, errorShower, 40, SpringLayout.NORTH, passwordField);

        mainLayout.putConstraint(SpringLayout.WEST, loginBtn, 200, SpringLayout.WEST, passwordLabel);
        mainLayout.putConstraint(SpringLayout.NORTH, loginBtn, 40, SpringLayout.NORTH, errorShower);

        loginBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String email = emailAddress.getText().toString().trim();
                String password = passwordField.getText().toString();
                if(email.equals("")){
                    errorShower.setText("Please enter your email address");
                }else if(password.equals("")){
                    errorShower.setText("Please enter your password");
                }else {
                    MainHttpCall cal = new MainHttpCall();
                   String body = "email="+email+"&password="+password;
                    StatusFeeder feeder = cal.POST(ProjectConstants.API_AUTH_URL+"login",body);
                    if(feeder.isStatus()){
                        JsonParser parser = new JsonParser();
                        JsonElement rootNode = parser.parse(feeder.getData());
                        Gson gson = new Gson();
                        System.out.println(rootNode.toString());
                        JButton button = (JButton)e.getSource();
                        Component component = (Component)button;
                        JFrame frame = (JFrame)SwingUtilities.getWindowAncestor(component);
                        LoginFrame loginFrame = (LoginFrame) frame;
                        loginFrame.setVisible(false);

                        CashiersDashboardFrame cashiersDashboardFrame = new CashiersDashboardFrame();
                        cashiersDashboardFrame.setVisible(true);
                        /*System.out.println(feeder.getData());
                        JsonParser parser = new JsonParser();
                        JsonElement rootNode = parser.parse(feeder.getData());
                        Gson gson = new Gson();
                        if(rootNode.isJsonObject()){
                            JsonObject details = rootNode.getAsJsonObject();
                            User user = gson.fromJson(details.getAsJsonObject("user").toString(), User.class);
                            if(user.getRole() != 2){
                                errorShower.setText("You are not eligible to login in this  app. maybe you can use the web app");
                            }else if(user.getRole()==2){
                                StatusFeeder feeder1 = cal.GET(ProjectConstants.API_URL+"no_auth_cashiers/"+user.getId()+"/", details.get("token").toString());
                                JsonObject object = JSONConvertor.convert(feeder1.getData());
                                JsonArray array = object.getAsJsonArray("data");
                                JsonElement element = array.get(0);
                                JsonObject objectElement = element.getAsJsonObject();
                                Cashiers cashiers = gson.fromJson(objectElement.toString(),Cashiers.class);
                                System.out.println(cashiers.getEmail());
                                 // finding current frame and close it then open dashboard frame
                                JButton button = (JButton)e.getSource();
                                Component component = (Component)button;
                                JFrame frame = (JFrame)SwingUtilities.getWindowAncestor(component);
                                LoginFrame loginFrame = (LoginFrame) frame;
                                loginFrame.setVisible(false);

                                // opening cashiers dashboard frame
                                CashiersDashboardFrame cashiersDashboardFrame = new CashiersDashboardFrame(cashiers);
                                cashiersDashboardFrame.setVisible(true);

                            }
                        }*/
                    }else {
                        errorShower.setText(feeder.getData());
                    }
                }
            }
        });


    }
}

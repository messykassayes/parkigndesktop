package com.loginComponents;

import javax.swing.*;
import java.awt.*;

public class LoginFrame extends JFrame {
    LoginPanel loginPanel;
    public LoginFrame(String title){

        setTitle(title);
        Dimension dimension = new Dimension(1000, 500);
        setSize(dimension);
        Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
        setLocation(dim.width/2-this.getSize().width/2, dim.height/2-this.getSize().height/2);

        // settting content pane
        loginPanel = new LoginPanel();
        setContentPane(loginPanel);
        setResizable(false);

    }
}

package com.models;

import com.google.gson.JsonObject;

import java.util.List;

public class StatusFeeder {
    private boolean status =false;
    private String data= null;
    private int code = 0;


    public StatusFeeder() {
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }
}

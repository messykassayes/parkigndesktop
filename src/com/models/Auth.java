package com.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Auth {
    @SerializedName("status")
    private String status =null;

    @SerializedName("token")
    private String token =null;

    @SerializedName("user")
    private List<User> user;

    @SerializedName("expires_in")
    private String expires_in =null;
    public Auth() {
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getExpires_in() {
        return expires_in;
    }

    public void setExpires_in(String expires_in) {
        this.expires_in = expires_in;
    }
}

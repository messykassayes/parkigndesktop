package com.models;

import com.google.gson.annotations.SerializedName;

public class Messages {
    @SerializedName("message")
    private String message = null;

    public Messages() {

    }

    public String getMessage() {
        return  message;
    }

    public void setMessage(String mess) {
        this.message = mess;
    }


}

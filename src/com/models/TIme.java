package com.models;

import com.google.gson.annotations.SerializedName;

public class TIme {
    @SerializedName("time")
    private String time;

    public TIme() {
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }
}

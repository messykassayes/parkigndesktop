package com.models;

import com.google.gson.annotations.SerializedName;

public class Slot {
    @SerializedName("id")
    private int id;

    @SerializedName("building_id")
    private int building_id;

    @SerializedName("floor_id")
    private int floor_id;

    @SerializedName("code")
    private String code;

    @SerializedName("is_reserved")
    private String is_reserved;

    @SerializedName("is_taken")
    private String is_taken;

    public Slot() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getBuilding_id() {
        return building_id;
    }

    public void setBuilding_id(int building_id) {
        this.building_id = building_id;
    }

    public int getFloor_id() {
        return floor_id;
    }

    public void setFloor_id(int floor_id) {
        this.floor_id = floor_id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getIs_reserved() {
        return is_reserved;
    }

    public void setIs_reserved(String is_reserved) {
        this.is_reserved = is_reserved;
    }

    public String getIs_taken() {
        return is_taken;
    }

    public void setIs_taken(String is_taken) {
        this.is_taken = is_taken;
    }
}

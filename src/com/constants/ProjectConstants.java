package com.constants;

public class ProjectConstants {
    public static String API_URL = "http://localhost:8000/api/";
    public static String API_AUTH_URL = "http://localhost:8000/api/auth/";
    public static String USER_AGENT = "Mozilla/5.0";
}

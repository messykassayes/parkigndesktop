/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cashiersDashboard.dataanalysis;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SpringLayout;

/**
 *
 * @author MESERET
 */
public class Show_Analysis extends JPanel{
    private JComboBox selected_road_name;
    private JLabel road_name;
    private SpringLayout layout;
   Data_table table;
    public Show_Analysis() {
        selected_road_name = new JComboBox();
        selected_road_name.setPreferredSize(new Dimension(250,20));
        selected_road_name.setForeground(Color.BLACK);
        selected_road_name.setFont(new Font("arial",Font.PLAIN,15));
        selected_road_name.addItem("Select road name");
        selected_road_name.addItem("Gotera mesalecha");
        selected_road_name.addItem("Meskel Adebabay");
        selected_road_name.addItem("Megenagna");
        selected_road_name.addItem("Wollo Sefer");
        
        table=new Data_table();
        
        road_name = new JLabel("select Road name: ");
        road_name.setForeground(Color.white);
        road_name.setFont(new Font("arial",Font.BOLD,15));
        
        layout = new SpringLayout();
        super.setLayout(layout);
        
        super.add(road_name);
        layout.putConstraint(SpringLayout.WEST, road_name, 50, SpringLayout.WEST, this);
        layout.putConstraint(SpringLayout.NORTH, road_name, 20, SpringLayout.NORTH, this);
        
        super.add(selected_road_name);
        layout.putConstraint(SpringLayout.WEST, selected_road_name, 150, SpringLayout.WEST, road_name);
        layout.putConstraint(SpringLayout.NORTH, selected_road_name, 20, SpringLayout.NORTH, this);
      
        
        selected_road_name.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e){
                Object ob=e.getSource();
                JComboBox box = (JComboBox)ob;
                String seleted_item= box.getSelectedItem().toString();
                add_table();
            }
        });
       
  
    }
    public void add_table(){
          super.add(table);
        layout.putConstraint(SpringLayout.WEST, table, 50, SpringLayout.WEST, this);
        layout.putConstraint(SpringLayout.NORTH, table, 100, SpringLayout.NORTH, selected_road_name);
   
    }
    
     @Override
    public void paintComponent(Graphics g) {
        Graphics2D g2 = (Graphics2D) g;
        g2.setColor(Color.decode("#242424"));
        g2.fillRect(0, 0, getWidth(), getHeight());
        g2.setColor(Color.decode("#5cb85c"));
        g2.drawRect(0, 0, getWidth() - 1, getHeight() - 1);

    }
}

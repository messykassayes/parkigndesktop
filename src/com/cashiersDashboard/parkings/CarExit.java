package com.cashiersDashboard.parkings;

import javax.swing.*;
import java.awt.*;

public class CarExit extends JPanel {

    public CarExit(){

    }

    @Override
    public void paintComponent(Graphics g) {
        Graphics2D g2 = (Graphics2D) g;
        g2.setColor(Color.decode("#242424"));
        g2.fillRect(0, 0, getWidth(), getHeight());
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cashiersDashboard.standards;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.geom.Ellipse2D;
import javax.swing.JPanel;

/**
 *
 * @author MESERET
 */
public class CircleButton extends JPanel {

    private String names;
    private Color circleColor = Color.decode("#5cb85c");
    private Color stringColor = Color.white;

    public CircleButton() {
        super.setPreferredSize(new Dimension(120, 120));
        super.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseEntered(MouseEvent me) {
                circleColor = Color.white;
                stringColor = Color.decode("#5cb85c");
                
                
            }

            @Override
            public void mouseExited(MouseEvent me) {
                circleColor = Color.decode("#5cb85c");
                stringColor = Color.white;
                
                
            }
        });

    }

    public void userName(String name) {
        names = name;
    }
    public CircleButton getCircleButton(){
      return this;
    
    }

    @Override
    public void paintComponent(Graphics g) {
        super.paintComponents(g);
        Graphics2D g2 = (Graphics2D) g;
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        g2.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
        Ellipse2D.Double hole = new Ellipse2D.Double();
        hole.width = super.getPreferredSize().width;
        hole.height = super.getPreferredSize().height;
        hole.x = 0;
        hole.y = 0;
        g2.setColor(circleColor);
        g2.fillOval((int) hole.x, (int) hole.y, (int) hole.width, (int) hole.height);
        g2.setColor(stringColor);
        g2.setFont(new Font("arial", Font.BOLD, 80));
        g2.drawString(names, 25, 85);
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cashiersDashboard.standards;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.ImageIcon;
import javax.swing.JButton;

/**
 *
 * @author messy
 */
public class StandardButton extends JButton {

    private String text;
    private Color defaultColor = Color.decode("#5cb85c");
    private boolean change = false;
    private ImageIcon icon;
    private boolean icons = false;
    private Font fonts = null;
    private Graphics2D g2;

    public StandardButton() {
        this.text = text;
        super.setFont(new Font("arial", Font.PLAIN, 12));
        super.setForeground(Color.black);
        super.setContentAreaFilled(false);
        super.setBorderPainted(false);
        super.setFocusPainted(false);
        super.setBorderPainted(false);
        super.setPreferredSize(new Dimension(240, 30));
        super.addMouseListener(new MouseLi());

    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Font getFonts() {
        return fonts;
    }

    public void setFonts(Font fonts) {
        this.fonts = fonts;
        super.setFont(fonts);
    }

    public Font getGraphicsFont() {
        return g2.getFont();
    }

    @Override
    public void paintComponent(Graphics g) {
        g2 = (Graphics2D) g;
        if (change) {
            g2.setColor(defaultColor);
            g2.fillRoundRect(0, 0, getWidth() - 1, getHeight() - 1, 5, 5);
            g2.setColor(Color.white);
            g2.setFont(getFonts());
            g2.drawString(text, 10, 20);
        } else {
            g2.setColor(Color.white);
            g2.fillRoundRect(0, 0, getWidth() - 1, getHeight() - 1, 5, 5);
            g2.setColor(Color.black);
            g2.setFont(getFonts());
            g2.drawString(text, 10, 20);
        }

    }

    class MouseLi extends MouseAdapter {

        @Override
        public void mouseEntered(MouseEvent me) {
            change = true;
        }

        @Override
        public void mouseExited(MouseEvent me) {
            change = false;
        }

    }

}

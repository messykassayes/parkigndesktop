/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cashiersDashboard.standards;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Insets;
import java.awt.Polygon;
import java.awt.Rectangle;
import javax.swing.JComponent;
import javax.swing.JTabbedPane;
import javax.swing.plaf.ComponentUI;
import javax.swing.plaf.basic.BasicTabbedPaneUI;

/**
 *
 * @author MESERET
 */
public class PPTTabbedPaneUI extends BasicTabbedPaneUI {

    private static final Insets TAB_INSETS = new Insets(1, 0, 0, 0);

    /**
     * The font to use for the selected tab
     */
    private Font boldFont;

    /**
     * The font metrics for the selected font
     */
    private FontMetrics boldFontMetrics;

    /**
     * The color to use to fill in the background
     */
    private Color selectedColor;

    /**
     * The color to use to fill in the background
     */
    private Color unselectedColor;

    // ------------------------------------------------------------------------------------------------------------------
    //  Custom installation methods
    // ------------------------------------------------------------------------------------------------------------------
    public static ComponentUI createUI(JComponent c) {
        return new PPTTabbedPaneUI();
    }
    
    protected void installDefaults() {
        super.installDefaults();
        tabAreaInsets.left = 10;
        selectedTabPadInsets = new Insets(0, 0, 0, 0);
        
        selectedColor = Color.decode("#5cb85c");
        unselectedColor = tabPane.getBackground().darker().darker();
        
        boldFont = tabPane.getFont().deriveFont(Font.BOLD);
        boldFontMetrics = tabPane.getFontMetrics(boldFont);
    }

    // ------------------------------------------------------------------------------------------------------------------
    //  Custom sizing methods
    // ------------------------------------------------------------------------------------------------------------------
    protected void paintTabArea(Graphics g, int tabPlacement, int selectedIndex) {
        int tw = tabPane.getBounds().width;

        g.setColor(Color.decode("#242424"));
        g.fillRect(0, 0, tw, rects[0].height + 3);

        super.paintTabArea(g, tabPlacement, selectedIndex);
    }
    public int getTabRunCount(JTabbedPane pane) {
        return 1;
    }
    
    protected Insets getContentBorderInsets(int tabPlacement) {
        return TAB_INSETS;
    }
    
    protected int calculateTabHeight(int tabPlacement, int tabIndex, int fontHeight) {
        int vHeight = fontHeight + 3;
        if (vHeight % 2 == 0) {
            vHeight += 1;
        }
        return vHeight;
    }
    
    protected int calculateTabWidth(int tabPlacement, int tabIndex, FontMetrics metrics) {
        return super.calculateTabWidth(tabPlacement, tabIndex, metrics) + metrics.getHeight() + 50;
    }

    // ------------------------------------------------------------------------------------------------------------------
    //  Custom painting methods
    // ------------------------------------------------------------------------------------------------------------------
    // ------------------------------------------------------------------------------------------------------------------
    //  Methods that we want to suppress the behaviour of the superclass
    // ------------------------------------------------------------------------------------------------------------------
    protected void paintTabBackground(Graphics g, int tabPlacement, int tabIndex, int x, int y, int w, int h, boolean isSelected) {
        Polygon shape = new Polygon();
        
        shape.addPoint(x - (h / 4), y + h);
        shape.addPoint(x + (h / 4), y);
        shape.addPoint(x + w - (h / 4), y);
        
        if (isSelected || (tabIndex == (rects.length - 1))) {
            if (isSelected) {
                g.setColor(Color.decode("#5cb85c"));
            } else {
                g.setColor(unselectedColor);
            }
            shape.addPoint(x + w + (h / 4), y + h);
        } else {
            g.setColor(unselectedColor);
            shape.addPoint(x + w, y + (h / 2));
            shape.addPoint(x + w - (h / 4), y + h);
        }
        
        g.fillPolygon(shape);
    }
    
    protected void paintTabBorder(Graphics g, int tabPlacement, int tabIndex, int x, int y, int w, int h, boolean isSelected) {
        g.setColor(Color.black);
        g.drawLine(x - (h / 4), y + h, x + (h / 4), y);
        g.drawLine(x + (h / 4), y, x + w - (h / 4), y);
        g.drawLine(x + w - (h / 4), y, x + w + (h / 4), y + h);
      
    }
 
    
    
    protected void paintContentBorderRightEdge(Graphics g, int tabPlacement, int selectedIndex, int x, int y, int w, int h) {
        
    }
    
    protected void paintContentBorderLeftEdge(Graphics g, int tabPlacement, int selectedIndex, int x, int y, int w, int h) {
        
    }
    
    protected void paintContentBorderBottomEdge(Graphics g, int tabPlacement, int selectedIndex, int x, int y, int w, int h) {
        
    }
    
    protected void paintFocusIndicator(Graphics g, int tabPlacement, Rectangle[] rects, int tabIndex, Rectangle iconRect, Rectangle textRect, boolean isSelected) {
        // Do nothing
    }
    
    protected void paintText(Graphics g, int tabPlacement, Font font, FontMetrics metrics, int tabIndex, String title, Rectangle textRect, boolean isSelected) {
        g.setColor(Color.white);
        g.setFont(new Font("nyala", Font.PLAIN, 15));
        
        if (isSelected) {
            int vDifference = (int) (boldFontMetrics.getStringBounds(title, g).getWidth()) - textRect.width;
            textRect.x -= (vDifference / 2);
             g.setColor(Color.white);
            g.setFont(new Font("nyala", Font.PLAIN, 15));
            super.paintText(g, tabPlacement, boldFont, boldFontMetrics, tabIndex, title, textRect, isSelected);
        } else {
             g.setColor(Color.white);
            super.paintText(g, tabPlacement, font, metrics, tabIndex, title, textRect, isSelected);
        }
    }
    
    protected int getTabLabelShiftY(int tabPlacement, int tabIndex, boolean isSelected) {
        return 0;
    }
}

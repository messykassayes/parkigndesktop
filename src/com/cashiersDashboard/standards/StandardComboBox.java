/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cashiersDashboard.standards;

/**
 *
 * @author messy
 */
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import javax.swing.FocusManager;
import javax.swing.JComboBox;

/**
 *
 * @author messy
 */
public class StandardComboBox extends JComboBox {

    private placeHolder holder;
    private String placeholder;

    public StandardComboBox(int column) {
        //super.setBackground(Color.white);
        super.setPreferredSize(new Dimension(column, 20));
        holder = new placeHolder(this);
    }

    public String getPlaceholder() {
        return placeholder;
    }

    public void setPlaceholder(String placeholder) {
        this.placeholder = placeholder;
    }

    @Override
    public void paint(Graphics g) {
        super.paint(g);
        holder.paint(g);
    }

    class placeHolder implements FocusListener {

        private StandardComboBox field;

        public placeHolder(StandardComboBox fi) {
            this.field = fi;
            field.addFocusListener(this);
        }

        @Override
        public void focusGained(FocusEvent e) {
            field.repaint();
        }

        @Override
        public void focusLost(FocusEvent e) {
            field.repaint();
        }

        public void paint(Graphics g) {
            Graphics2D g2 = (Graphics2D) g.create();
            if (field.getSelectedItem().toString().equals("") && (FocusManager.getCurrentKeyboardFocusManager().getFocusOwner() != field)) {

                g2.setFont(new Font("arial", Font.PLAIN, 15));
                g2.setColor(Color.gray);
                g2.drawString(field.getPlaceholder(), 70, 15);
            }
        }
    }
}

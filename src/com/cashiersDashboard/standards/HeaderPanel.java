/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cashiersDashboard.standards;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GradientPaint;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.util.Arrays;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 *
 * @author messy
 */
public class HeaderPanel extends JPanel {

    private JPanel westPanel = new JPanel() {
        @Override
        public void paintComponent(Graphics g) {
            GradientPaint gradient = new GradientPaint(0, 0, Color.decode("#434343"), 0, getHeight(), Color.black);
            Graphics2D g2 = (Graphics2D) g;
            g2.setPaint(gradient);
            g2.fillRect(0, 0, getWidth() - 1, getHeight() - 1);
        }
    };
    private JPanel estPanel = new JPanel() {
        @Override
        public void paintComponent(Graphics g) {
            GradientPaint gradient = new GradientPaint(0, 0, Color.decode("#434343"), 0, getHeight(), Color.black);
            Graphics2D g2 = (Graphics2D) g;
            g2.setPaint(gradient);
            g2.fillRect(0, 0, getWidth() - 1, getHeight() - 1);
        }
    };

    private JLabel choices;
    private StandardLabel setting, file, help, signOut;
    private boolean add = false;

    public HeaderPanel(boolean ad) {
        this.add = ad;
        super.setPreferredSize(new Dimension(20, 25));
        westPanel.setLayout(new FlowLayout(FlowLayout.LEADING, 10, 5));
        estPanel.setLayout(new FlowLayout(FlowLayout.LEADING, 10, 5));
        // bar.setPreferredSize(new Dimension(500, 20));
        super.setLayout(new BorderLayout());
        super.add(westPanel, BorderLayout.WEST);
        super.add(estPanel, BorderLayout.EAST);
        defaultLangeuage();

    }

    public void addMenus(StandardLabel files, StandardLabel sett) {
        this.setting = sett;
        this.file = files;

        setting.setForeground(Color.white);
        file.setForeground(Color.white);
        if (add) {
            westPanel.add(setting);
            westPanel.add(file);
        }
    }

    public void addSingle(StandardLabel helps) {
        this.help = helps;
        help.setForeground(Color.white);
        if (add) {
            westPanel.add(help);
        }
    }

    public void addTeEst(StandardLabel signOuts) {
        this.signOut = signOuts;
        signOuts.setForeground(Color.white);
        estPanel.add(signOut);

    }

    public void defaultLangeuage() {
    }

    public void changeLanguage() {
        for (JComponent comp : Arrays.asList(setting, help)) {
            comp.setFont(new Font("nyala", Font.PLAIN, 16));
            comp.setForeground(Color.white);
        }
        setting.setText("ማስተካከያ");
        help.setText("እርዳታ");
    }

    public void paintComponent(Graphics g) {
        GradientPaint gradient = new GradientPaint(0, 0, Color.decode("#434343"), 0, getHeight(), Color.black);
        Graphics2D g2 = (Graphics2D) g;
        g2.setPaint(gradient);
        g2.fillRect(0, 0, getWidth() - 1, getHeight() - 1);
    }
}

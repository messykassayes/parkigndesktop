/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cashiersDashboard.standards;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import javax.swing.BorderFactory;
import javax.swing.FocusManager;
import javax.swing.JPasswordField;

/**
 *
 * @author messy
 */
public class StandardPasswordField extends JPasswordField {

    private placeHolder holder;
    private String placeholder;
    private Font fonts;

    public StandardPasswordField(int column, int w, int h) {
        super.setColumns(column);
        super.setPreferredSize(new Dimension(w, h));
        //super.setBackground(Color.white);
        holder = new placeHolder(this);
    }

    public Font getFonts() {
        return fonts;
    }

    public void setFonts(Font fonts) {
        this.fonts = fonts;
    }

    public String getPlaceholder() {
        return placeholder;
    }

    public void setPlaceholder(String placeholder) {
        this.placeholder = placeholder;
    }

    @Override
    public void paint(Graphics g) {
        super.paint(g);
        holder.paint(g);
    }

    class placeHolder implements FocusListener {

        private StandardPasswordField field;

        public placeHolder(StandardPasswordField fi) {
            this.field = fi;
            field.addFocusListener(this);
        }

        @Override
        public void focusGained(FocusEvent e) {
            field.repaint();
        }

        @Override
        public void focusLost(FocusEvent e) {
            field.repaint();
        }

        public void paint(Graphics g) {
            Graphics2D g2 = (Graphics2D) g.create();
            if (field.getText().isEmpty() && (FocusManager.getCurrentKeyboardFocusManager().getFocusOwner() != field)) {

                g2.setFont(field.getFonts());
                g2.setColor(Color.gray);
                g2.drawString(field.getPlaceholder(), 70, 18);
                field.setBorder(BorderFactory.createEtchedBorder(Color.black, Color.black));

            } else if (field.getText().equals("") && (FocusManager.getCurrentKeyboardFocusManager().getFocusOwner() == field)) {

                field.setBorder(BorderFactory.createEtchedBorder(Color.decode("#e89271"), Color.decode("#e89271")));

            }
        }
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cashiersDashboard.standards;

import com.cashiersDashboard.listeners.MainActionListener;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.Window;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Arrays;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

/**
 *
 * @author messy
 */
public class StandardFrame extends JFrame {

    private Color titleColor = Color.decode("#040404");
    private SideLabel northLabel = new SideLabel(Side.N);
    private SideLabel westLabel = new SideLabel(Side.W);
    private SideLabel estLabel = new SideLabel(Side.E);
    private SideLabel northWestLabel = new SideLabel(Side.NW);
    private SideLabel northEastLabel = new SideLabel(Side.NE);
    private SideLabel southLabel = new SideLabel(Side.S);
    private SideLabel southwest = new SideLabel(Side.SW);
    private SideLabel southest = new SideLabel(Side.SE);
    private BorderLayout layout;
    private JPanel contetPane = new JPanel();
    private JPanel northPanel = new JPanel() {
        public void paintComponent(Graphics g) {
            Graphics2D g2 = (Graphics2D) g;
            g2.setColor(titleColor);
            g2.fillRect(0, 0, getWidth(), getHeight());
        }

    };
    private JPanel titlePanel = new JPanel() {
        public void paintComponent(Graphics g) {
            Graphics2D g2 = (Graphics2D) g;
            g2.setColor(titleColor);
            g2.fillRect(0, 0, getWidth(), getHeight());
        }

    };
    private JPanel buttonsPanel = new JPanel() {
        public void paintComponent(Graphics g) {
            Graphics2D g2 = (Graphics2D) g;
            g2.setColor(titleColor);
            g2.fillRect(0, 0, getWidth(), getHeight());
        }

    };
    private JPanel southPanel = new JPanel() {
        public void paintComponent(Graphics g) {
            Graphics2D g2 = (Graphics2D) g;
            g2.setColor(titleColor);
            g2.fillRect(0, 0, getWidth(), getHeight());
        }

    };
    private ResizeListener rsl;
    private DrageListner dgl;
    private JLabel iconLabel, titleLabel;
    private JButton minimize, maximize, close;
    private MMCbuttons MMC;
    private MainActionListener actionListener;
    private ImageIcon icon, titleICon;

    public StandardFrame() {
        rsl = new ResizeListener(this);
        actionListener = new MainActionListener();
        layout = new BorderLayout();
        contetPane.setLayout(layout);
        //setting and adding components to north panel
        northPanel.setLayout(new BorderLayout());

        MMC = new MMCbuttons();
        minimize = MMC.minimizeButton();
        minimize.setActionCommand("minimize");
        minimize.addActionListener(actionListener);
        maximize = MMC.maximizeButton();
        maximize.setActionCommand("maximize");
        maximize.addActionListener(actionListener);
        close = MMC.closeButton();
        close.setActionCommand("close");
        close.addActionListener(actionListener);
        buttonsPanel.setLayout(new FlowLayout(FlowLayout.LEFT, 10, 5));

        icon = new ImageIcon(this.getClass().getResource("/com/images/title.png"));
        titleICon = new ImageIcon(this.getClass().getResource("/com/images/gcIcon.png"));
        iconLabel = new JLabel(icon);
        iconLabel.setForeground(Color.white);
        titleLabel = new JLabel("GuloCam", JLabel.CENTER);
        titleLabel.setForeground(Color.white);
        titleLabel.setFont(new Font("arial", Font.PLAIN, 14));
        titlePanel.setLayout(new BorderLayout());
        titlePanel.add(iconLabel, BorderLayout.WEST);
        titlePanel.add(titleLabel, BorderLayout.CENTER);
        titlePanel.add(buttonsPanel, BorderLayout.EAST);

        northPanel.setPreferredSize(new Dimension(100, 25));
        northPanel.add(northLabel, BorderLayout.NORTH);
        northPanel.add(northWestLabel, BorderLayout.WEST);
        northPanel.add(northEastLabel, BorderLayout.EAST);
        northPanel.add(titlePanel, BorderLayout.CENTER);
        dgl = new DrageListner();
        northPanel.addMouseMotionListener(dgl);
        northPanel.addMouseListener(dgl);
        //adding components to south pane

        southPanel.setLayout(new BorderLayout());
        southPanel.add(southLabel, BorderLayout.SOUTH);
        southPanel.add(southest, BorderLayout.EAST);
        southPanel.add(southwest, BorderLayout.WEST);
        //adding Components to contentpane

        westLabel.setBackground(titleColor);
        contetPane.setBackground(titleColor);
        contetPane.add(westLabel, BorderLayout.WEST);
        contetPane.add(estLabel, BorderLayout.EAST);
        contetPane.add(northPanel, BorderLayout.NORTH);
        contetPane.add(southPanel, BorderLayout.SOUTH);
        super.setContentPane(contetPane);
        super.setIconImage(titleICon.getImage());
        super.setUndecorated(true);
        //super.setSize(500, 500);
        for (SideLabel side : Arrays.asList(westLabel, estLabel, northLabel, southest, southLabel, southwest, northEastLabel, northWestLabel)) {
            side.addMouseListener(rsl);
            side.addMouseMotionListener(rsl);
        }
    }

    public void addContentPane(JComponent comp) {
        contetPane.add(comp, BorderLayout.CENTER);
    }

    public void addButtons(boolean add) {
        if (add) {
            buttonsPanel.add(minimize);
            buttonsPanel.add(maximize);
            buttonsPanel.add(close);
        } else {
            buttonsPanel.add(minimize);
            buttonsPanel.add(close);
        }
    }

    class DrageListner extends MouseAdapter {

        private Point startPoint = new Point();
        private Window w;

        @Override
        public void mousePressed(MouseEvent me) {
            Object o = me.getSource();
            if (o instanceof Window) {
                w = (Window) o;

            } else if (o instanceof JComponent) {
                JPanel pane = (JPanel) o;
                w = SwingUtilities.getWindowAncestor(pane);
            }
            startPoint.setLocation(me.getPoint());
        }

        @Override
        public void mouseDragged(MouseEvent me) {
            Point p = me.getLocationOnScreen();
            if (w != null) {
                w.setLocation(p.x - startPoint.x, p.y - startPoint.y);
            }

        }
    }

    enum Side {

        N(Cursor.N_RESIZE_CURSOR, new Dimension(0, 4)),
        S(Cursor.S_RESIZE_CURSOR, new Dimension(0, 4)),
        E(Cursor.E_RESIZE_CURSOR, new Dimension(4, 0)),
        W(Cursor.W_RESIZE_CURSOR, new Dimension(4, 0)),
        NE(Cursor.NE_RESIZE_CURSOR, new Dimension(4, 4)),
        NW(Cursor.NW_RESIZE_CURSOR, new Dimension(4, 4)),
        SE(Cursor.SE_RESIZE_CURSOR, new Dimension(4, 4)),
        SW(Cursor.SW_RESIZE_CURSOR, new Dimension(4, 4));

        public final int cursor;
        public final Dimension dims;

        private Side(int cu, Dimension d) {
            this.cursor = cu;
            this.dims = d;
        }

    }

    class SideLabel extends JLabel {

        private Side side;

        public SideLabel(Side side) {
            this.side = side;
            super.setCursor(Cursor.getPredefinedCursor(side.cursor));
            super.setBackground(Color.black);
        }

        @Override
        public Dimension getPreferredSize() {
            return side.dims;
        }

        @Override
        public Dimension getMaximumSize() {
            return side.dims;
        }

        @Override
        public Dimension getMinimumSize() {
            return side.dims;
        }

    }

    class ResizeListener extends MouseAdapter {

        private JFrame frame;
        private Rectangle rect;

        public ResizeListener(JFrame frame) {
            this.frame = frame;
        }

        @Override
        public void mousePressed(MouseEvent me) {
            rect = frame.getBounds();
        }

        @Override
        public void mouseDragged(MouseEvent me) {
            Side side = ((SideLabel) me.getSource()).side;
            frame.setBounds(getRectangle(rect, side, me.getX(), me.getY()));
        }

        public Rectangle getRectangle(Rectangle r, Side s, int dx, int dy) {
            switch (s) {
                case N:
                    r.y += dy;
                    r.height -= dy;
                    break;
                case S:
                    r.height += dy;
                    break;
                case W:
                    r.x += dx;
                    r.width -= dx;
                    break;
                case E:
                    r.width += dx;
                    break;
                case SE:
                    r.width += dx;
                    r.height += dy;
                    break;
                case SW:
                    r.x += dx;
                    r.width -= dx;
                    r.height += dy;
                    break;
                case NW:
                    r.x += dx;
                    r.width -= dx;
                    r.y += dy;
                    r.height -= dy;
                    break;
                case NE:
                    r.width += dx;
                    break;

            }
            return r;
        }
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cashiersDashboard.standards;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Frame;
import java.awt.GradientPaint;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Window;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.font.TextAttribute;
import java.util.Map;
import javax.swing.BorderFactory;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.SpringLayout;
import javax.swing.SwingUtilities;

/**
 *
 * @author messy
 */
public class StandardLabel extends JLabel {

    private JPanel barPanel = new JPanel() {
        @Override
        public void paintComponent(Graphics g) {
            Graphics2D g2 = (Graphics2D) g;
            g2.setColor(Color.black);
            g2.fillRect(0, 0, getWidth() - 1, getHeight() - 1);
        }
    };
    
    private JPopupMenu bar = new JPopupMenu() {
        @Override
        public void paintComponent(Graphics g) {
            GradientPaint gradient = new GradientPaint(0, 0, Color.decode("#434343"), 0, getHeight(), Color.black);
            Graphics2D g2 = (Graphics2D) g;
            g2.setColor(Color.black);
            g2.fillRect(0, 0, getWidth(), getHeight());
        }
    };
    private JPopupMenu bar2 = new JPopupMenu() {
        @Override
        public void paintComponent(Graphics g) {
            GradientPaint gradient = new GradientPaint(0, 0, Color.decode("#434343"), 0, getHeight(), Color.black);
            Graphics2D g2 = (Graphics2D) g;
            g2.setColor(Color.black);
            g2.fillRect(0, 0, getWidth(), getHeight());
        }
    };
    
    private Font original;
    private Font o = null;
    private boolean addPopUp;
    private JMenu changeLanguage;
    private StandardLabel2 signOut;
    SpringLayout layout;
    public StandardLabel() {
        super.addMouseListener(new Ml());
        super.addMouseMotionListener(new Ml());
        bar.setBorder(BorderFactory.createLineBorder(Color.decode("#5cb85c"), 1));
        bar2.setPreferredSize(new Dimension(140, 180));
        bar2.setBorder(BorderFactory.createLineBorder(Color.decode("#5cb85c"), 1));
        layout = new SpringLayout();
        barPanel.setLayout(layout);
        signOut = new StandardLabel2();
        signOut.setFont(new Font("arial",Font.PLAIN,18));
        signOut.setForeground(Color.white);
        signOut.setText("<html><h2>&nbsp&nbsp&nbsp&nbsp Log Out</h2></html>");
        bar2.add(barPanel);
        
    }
    
    public Font getOriginal() {
        return original;
    }
    
    public boolean isAddPopUp() {
        return addPopUp;
    }
    
    public void setAddPopUp(boolean addPopUp) {
        this.addPopUp = addPopUp;
    }
    
    public void addMenusItems(JMenu change) {
        this.changeLanguage = change;
        bar.add(changeLanguage);
        
    }
    
    public void addMenuItem(JMenuItem item) {
        bar.add(item);
    }
    
    public void addSignOutData(CircleButton button) {
        barPanel.add(button);
        layout.putConstraint(SpringLayout.WEST, button, 10, SpringLayout.WEST, barPanel);
        layout.putConstraint(SpringLayout.NORTH, button, 10, SpringLayout.WEST, barPanel);
        barPanel.add(signOut);
        layout.putConstraint(SpringLayout.WEST, signOut, 15, SpringLayout.WEST, barPanel);
        layout.putConstraint(SpringLayout.NORTH, signOut, 120, SpringLayout.WEST, button);
        
    }
    
    public void setOriginal(Font original) {
        super.setFont(original);
        this.original = original;
    }
    
    class Ml extends MouseAdapter {
        
        private JLabel label;
        
        @Override
        public void mouseClicked(MouseEvent e) {
            JComponent comp = (JComponent) e.getSource();
            JLabel label = (JLabel) comp;
            if (addPopUp) {
                bar.show(e.getComponent(), label.getLocation().x - 13, label.getLocation().y + 12);
               
            } else if (label.getText().equals("<html><span>&#9660;</span></html>")) {
                Window window = SwingUtilities.getWindowAncestor(label);
                JFrame frame = (JFrame) window;
                if (frame.getExtendedState() != Frame.MAXIMIZED_BOTH) {
                    bar2.show(e.getComponent(), label.getLocation().x - 110, label.getLocation().y + 12);
                } else {
                    bar2.show(e.getComponent(), label.getLocation().x - 13, label.getLocation().y + 12);
                }
            }
        }
        
        @Override
        public void mouseEntered(MouseEvent e) {
            JComponent comp = (JComponent) e.getSource();
            label = (JLabel) comp;
            o = label.getFont();
            Map attributes = o.getAttributes();
            attributes.put(TextAttribute.UNDERLINE, TextAttribute.UNDERLINE_ON);
            label.setFont(original.deriveFont(attributes));
        }
        
        @Override
        public void mouseExited(MouseEvent e) {
            label.setFont(original);
        }
        
    }
    
}

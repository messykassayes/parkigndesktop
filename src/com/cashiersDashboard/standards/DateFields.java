/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 *//*

package com.cashiersDashboard.standards;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import javax.swing.BorderFactory;
import javax.swing.FocusManager;
import org.jdesktop.swingx.JXDatePicker;

*/
/**
 *
 * @author messy
 *//*

public class DateFields extends JXDatePicker {

    private DateFormat format = new SimpleDateFormat("dd/MM/yyyy");
    private String placeHolder;
    private Holder holder;

    public DateFields(int column) {
        super.setFormats(format);
        holder = new Holder(this);
        super.setPreferredSize(new Dimension(column, 20));
    }

    public String getPlaceHolder() {
        return placeHolder;
    }

    public void setPlaceHolder(String placeHolder) {
        this.placeHolder = placeHolder;
    }

    public void paint(Graphics g) {
        super.paint(g);
        holder.paint(g);

    }

    class Holder implements FocusListener {

        private DateFields field;

        public Holder(DateFields field) {
            this.field = field;
            this.field.addFocusListener(this);
        }

        @Override
        public void focusGained(FocusEvent e) {
            field.repaint();
        }

        @Override
        public void focusLost(FocusEvent e) {
            field.repaint();
        }

        public void paint(Graphics g) {
            Graphics2D g2 = (Graphics2D) g.create();
            if (field.getDate() == null && (FocusManager.getCurrentKeyboardFocusManager().getFocusOwner() != field)) {
                g2.setFont(new Font("arial", Font.PLAIN, 15));
                g2.setColor(Color.gray);
                g2.drawString(field.getPlaceHolder(), 70, 15);
            } else if (FocusManager.getCurrentKeyboardFocusManager().getFocusOwner() == field) {

                field.setBorder(BorderFactory.createEtchedBorder(Color.decode("#e89271"), Color.decode("#e89271")));

            }
        }

    }

}
*/

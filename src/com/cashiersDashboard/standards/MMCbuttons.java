/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cashiersDashboard.standards;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.JButton;

/**
 *
 * @author messy
 */
public class MMCbuttons {
    
    private Color buttonsColor = Color.decode("#5E6F4D");
    private MouseLi ml;
    private boolean change = false;
    
    public MMCbuttons() {
        ml = new MouseLi(this);
    }
    
    public JButton minimizeButton() {
        JButton minimize = new JButton(new MinimizeIcon());
        minimize.setToolTipText("Minimize");
        minimize.setBorderPainted(false);
        minimize.setContentAreaFilled(false);
        minimize.setFocusPainted(false);
        minimize.setBorder(BorderFactory.createEmptyBorder());
        minimize.addMouseListener(ml);
        
        return minimize;
        
    }
    
    public JButton maximizeButton() {
        JButton maximize = new JButton(new MaximizeIcon());
        maximize.setBorderPainted(false);
        maximize.setContentAreaFilled(false);
        maximize.setFocusPainted(false);
        maximize.setBorder(BorderFactory.createEmptyBorder());
        maximize.addMouseListener(ml);
        return maximize;
        
    }
    
    public JButton closeButton() {
        JButton close = new JButton(new CloseIcon());
        close.setToolTipText("Close");
        close.setBorderPainted(false);
        close.setContentAreaFilled(false);
        close.setFocusPainted(false);
        close.setBorder(BorderFactory.createEmptyBorder());
        close.addMouseListener(ml);
        return close;
        
    }
    
    class CloseIcon implements Icon {
        
        @Override
        public void paintIcon(Component cmpnt, Graphics grphcs, int i, int i1) {
            Graphics2D g2 = (Graphics2D) grphcs.create();
            g2.setStroke(new BasicStroke(2, BasicStroke.JOIN_BEVEL, BasicStroke.JOIN_BEVEL));
            if (change) {
                g2.setColor(buttonsColor);
                g2.drawLine(0, 0, 10, 10);
                g2.drawLine(0, 10, 10, 0);
            } else {
                g2.setColor(Color.white);
                g2.drawLine(0, 0, 10, 10);
                g2.drawLine(0, 10, 10, 0);
            }
            
        }
        
        @Override
        public int getIconWidth() {
            return 14;
        }
        
        @Override
        public int getIconHeight() {
            return 12;
        }
        
    }
    
    class MinimizeIcon implements Icon {
        
        @Override
        public void paintIcon(Component c, Graphics g, int x, int y) {
            Graphics2D g2 = (Graphics2D) g;
            g2.setStroke(new BasicStroke(3, BasicStroke.JOIN_BEVEL, BasicStroke.JOIN_BEVEL));
            if (change) {
                g2.setColor(buttonsColor);
                g2.drawLine(2, 6, 12, 6);
            } else {
                g2.setColor(Color.white);
                g2.drawLine(2, 6, 12, 6);
            }
        }
        
        @Override
        public int getIconWidth() {
            return 14;
        }
        
        @Override
        public int getIconHeight() {
            return 12;
        }
        
    }
    
    class MaximizeIcon implements Icon {
        
        @Override
        public void paintIcon(Component c, Graphics g, int x, int y) {
            Graphics2D g2 = (Graphics2D) g;
            g2.setStroke(new BasicStroke(2, BasicStroke.JOIN_BEVEL, BasicStroke.JOIN_BEVEL));
            if (change) {
                g2.setColor(buttonsColor);
                g2.drawRect(2, 2, 12, 10);
            } else {
                g2.setColor(Color.white);
                g2.drawRect(2, 2, 12, 10);
            }
        }
        
        @Override
        public int getIconWidth() {
            return 14;
        }
        
        @Override
        public int getIconHeight() {
            return 12;
        }
        
    }
    
    class MouseLi extends MouseAdapter {
        
        private MMCbuttons mmc;
        
        public MouseLi(MMCbuttons mmc) {
            this.mmc = mmc;
        }
        
        @Override
        public void mouseEntered(MouseEvent me) {
            change = true;
        }
        
        @Override
        public void mouseExited(MouseEvent me) {
            change = false;
        }
        
        public void mousePressed(MouseEvent me) {
            change = false;
       
        }
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cashiersDashboard.standards;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.border.EtchedBorder;

/**
 *
 * @author MESERET
 */
public class StandardTabPane extends JTabbedPane {

    private PPTTabbedPaneUI ui;

    private TabCloseUI close;
    public int intialX;
    public int initailY;
    private final int w = 8, h = 8;
    public Rectangle rectangle = new Rectangle(0, 0, w, h);
    public int upperLineX = 110;
    public int upperLineY = 5;
    public int downLinex = 105;
    public int downLineY = 10;
    public int length = 115;
    public int height = 15;
    private Color color;

    public StandardTabPane() {
        //  framePanel = new MainFrameContentPane();

        close = new TabCloseUI(this);
        color = new Color(0, 0, 0);
        super.setPreferredSize(new Dimension(300, 400));
        ui = new PPTTabbedPaneUI();
        super.setFont(new Font("nyala", Font.PLAIN, 15));
        super.setUI(ui);
        super.setBorder(BorderFactory.createEtchedBorder(EtchedBorder.RAISED, Color.black, Color.black));
        super.setForeground(Color.black);
        super.setBackground(Color.black);
        if (tabsNo()) {
            rectangle.x = downLinex;
            rectangle.y = upperLineY;
        }
    }

    public boolean tabsNo() {
        if (this.getTabCount() > -1) {
            return true;
        }
        return false;
    }

    public void changeBack() {
        for (int i = 0; i < super.getTabCount(); i++) {
            super.setBackgroundAt(i, Color.white);
        }

    }

    @Override
    public void paint(Graphics g) {
        super.paint(g);

        close.paint(g);
        //super.repaint();
    }

    public void TabAdder(Graphics g) {
        if (tabsNo()) {
            Graphics2D g2 = (Graphics2D) g;
            g2.setStroke(new BasicStroke(2, BasicStroke.JOIN_BEVEL, BasicStroke.JOIN_BEVEL));
            g2.setColor(Color.yellow);
            g2.drawLine(upperLineX, upperLineY, upperLineX, height);
            g2.drawLine(downLinex, downLineY, length, downLineY);

        }

    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    class TabCloseUI implements MouseListener, MouseMotionListener {

        private StandardTabPane tabbedPane;
        private int mex = 0;
        private int mey = 0;
        private int closex = 0;
        private int closey = 0;
        private final int width = 8;
        private final int height = 8;
        private int selectedTab;
        private Rectangle rect = new Rectangle(0, 0, width, height);

        public TabCloseUI(StandardTabPane tab) {
            this.tabbedPane = tab;
            tabbedPane.addMouseListener(this);
            tabbedPane.addMouseMotionListener(this);

        }

        @Override
        public void mouseClicked(MouseEvent me) {
        }

        @Override
        public void mousePressed(MouseEvent me) {
        }

        @Override
        public void mouseEntered(MouseEvent me) {
        }

        @Override
        public void mouseExited(MouseEvent me) {
        }

        @Override
        public void mouseDragged(MouseEvent me) {
        }

        @Override
        public void mouseReleased(MouseEvent me) {
            if (closeUnderMouse(me.getX(), me.getY())) {
                boolean isSelectedToClose = true;
                if (isSelectedToClose && selectedTab > -1) {
                    // JOptionPane.showMessageDialog(null, "Are you sure you want to Close" + tabbedPane.getTitleAt(selectedTab), "information ", JOptionPane.INFORMATION_MESSAGE);
                    tabbedPane.removeTabAt(selectedTab);

                    tabbedPane.setColor(Color.black);
                } else if (isSelectedToClose && tabbedPane.getTabCount() == 1) {
                    Container con = tabbedPane.getParent();
                    JPanel cv = (JPanel) con;
                    cv.remove(tabbedPane);
                    cv.repaint();
                    cv.validate();
                    // framePanel.removeTab();
                }
                selectedTab = tabbedPane.getSelectedIndex();
            }
        }

        @Override
        public void mouseMoved(MouseEvent me) {
            mex = me.getX();
            mey = me.getY();
            if (mouseOverTab(mex, mey)) {
                controlCursor();
                tabbedPane.repaint();
            } else if (me.getX() == tabbedPane.upperLineX && me.getY() == tabbedPane.upperLineY) {
            }

        }

        public void controlCursor() {
            if (tabbedPane.getTabCount() > 0) {
                if (closeUnderMouse(mex, mey)) {
                    tabbedPane.setCursor(new Cursor(Cursor.HAND_CURSOR));
                    tabbedPane.setToolTipTextAt(selectedTab, "Close   " + tabbedPane.getTitleAt(selectedTab));

                } else {
                    tabbedPane.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
                    tabbedPane.setToolTipTextAt(selectedTab, tabbedPane.getTitleAt(selectedTab));

                }

            }

        }

        public boolean closeUnderMouse(int x, int y) {
            rect.x = closex-5;
            rect.y = closey-1;
            return rect.contains(x, y);
        }

        public void paint(Graphics g) {
            int tabCount = tabbedPane.getTabCount();
            for (int j = 0; j < tabCount; j++) {
                if (tabbedPane.getComponent(j).isShowing()) {
                    int x = tabbedPane.getBoundsAt(j).x + tabbedPane.getBoundsAt(j).width - width - 5;
                    int y = tabbedPane.getBoundsAt(j).y + 5;
                    drawClose(g, x, y);
                    break;

                }

            }
            if (mouseOverTab(mex, mey)) {
                drawClose(g, closex, closey);
            }

        }

        public void drawClose(Graphics g, int x, int y) {
            if (tabbedPane.getTabCount() > 0) {
                Graphics2D g2 = (Graphics2D) g;
                drawColored(g2, isUnderMouse(x, y) ? Color.WHITE : Color.red, x, y);
            }

        }

        public void drawColored(Graphics2D g2, Color c, int x, int y) {
            Rectangle rect = new Rectangle(x - 9, y - 1, width + 5, height + 5);
            if (c == Color.WHITE) {
                g2.setColor(Color.white);
                g2.fillRect(rect.x, rect.y, rect.width, rect.height);
            }
            g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
            g2.setStroke(new BasicStroke(2, BasicStroke.JOIN_BEVEL, BasicStroke.JOIN_BEVEL));
            g2.setColor(Color.black);
            g2.setFont(new Font("arial", Font.BOLD, 16));
            g2.drawString("x", rect.x+1, rect.y+12);
            //g2.drawLine(rect.x + 2, rect.y + 2, rect.x + width, rect.y + height + 2);
            //g2.drawLine(rect.x + 2, rect.y + rect.height - 2, rect.x + rect.width - 2, rect.y + 2);
            g2.setColor(Color.black);
            g2.setFont(new Font("arial", Font.BOLD, 18));
           // g2.drawLine(rect.x + 2, rect.y + 2, rect.x + width, rect.y + height + 2);
            //g2.drawLine(rect.x + 2, rect.y + rect.height - 2, rect.x + rect.width - 2, rect.y + 2);
           g2.drawString("x", rect.x+1, rect.y+12);
        }

        public boolean isUnderMouse(int x, int y) {
            if (Math.abs(x - mex) < width && Math.abs(y - mey) < height) {
                return true;
            }
            return false;

        }

        public boolean mouseOverTab(int x, int y) {
            int tabCount = tabbedPane.getTabCount();
            for (int i = 0; i < tabCount; i++) {
                if (tabbedPane.getBoundsAt(i).contains(mex, mey)) {
                    selectedTab = i;
                    closex = tabbedPane.getBoundsAt(i).x + tabbedPane.getBoundsAt(i).width - width - 5;
                    closey = tabbedPane.getBoundsAt(i).y + 5;
                    return true;

                }

            }
            return false;

        }

    }

}

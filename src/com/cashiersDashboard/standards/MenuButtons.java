/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cashiersDashboard.standards;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.font.TextAttribute;
import java.util.Map;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComponent;

/**
 *
 * @author messy
 */
public class MenuButtons extends JButton {

    private Font orginal;
    private Color foreground;

    public MenuButtons() {
        super.setForeground(foreground);
        super.setFont(orginal);
        super.setContentAreaFilled(false);
        super.setFocusPainted(false);
        super.setBorder(BorderFactory.createEmptyBorder());
        super.addMouseListener(new ML());
    }

    public void setforeGrounds(Color color) {
        foreground = color;
        super.setForeground(color);

    }

    public void addFont(Font f) {
        this.orginal = f;
        super.setFont(orginal);
    }

    class ML extends MouseAdapter {

        private JButton b;

        @Override
        public void mouseEntered(MouseEvent me) {
            JComponent c = (JComponent) me.getComponent();
            b = (JButton) c;
            Map attributes = b.getFont().getAttributes();
            attributes.put(TextAttribute.UNDERLINE, TextAttribute.UNDERLINE_ON);
            b.setFont(orginal.deriveFont(attributes));

        }

        @Override
        public void mouseExited(MouseEvent me) {
            b.setFont(orginal);
        }

    }

}

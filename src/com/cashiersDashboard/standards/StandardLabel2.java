/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cashiersDashboard.standards;

import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.font.TextAttribute;
import java.util.Map;
import javax.swing.JComponent;
import javax.swing.JLabel;

/**
 *
 * @author MESERET
 */
public class StandardLabel2 extends JLabel{
    private Font original;

    public StandardLabel2() {
        super.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        super.addMouseListener(new MouseAdapter(){
        JLabel button;
        @Override
        public void mouseEntered(MouseEvent me){
            JComponent c = (JComponent)me.getSource();
             button = (JLabel)c;
            original = button.getFont();
            Map attribute = original.getAttributes();
            attribute.put(TextAttribute.UNDERLINE, TextAttribute.UNDERLINE_ON);
            button.setFont(original.deriveFont(attribute));
        }
        @Override
        public void mouseExited(MouseEvent me){
         button.setFont(original);
        }
        
        });  
    }
    
    
}

package com.cashiersDashboard.mains;

import com.models.Cashiers;
import com.models.Floor;
import com.models.TIme;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class CarRegistrationFrame extends JFrame {
   private JPanel mainPanel = new JPanel(){
       @Override
       public void paintComponent(Graphics g) {
           Graphics2D g2 = (Graphics2D) g;
           g2.setColor(Color.decode("#242424"));
           g2.fillRect(0, 0, getWidth(), getHeight());
       }
   };
   public static final int N =10;
   private JLabel placeName,floorName, entranceTime,plateNumberLabel,slotCodeLabel;
   private JComboBox slots;
   private JTextField plateNumber;
   private JPanel platePanel = new JPanel(){
       public void paintComponent(Graphics g) {
           Graphics2D g2 = (Graphics2D) g;
           g2.setColor(Color.decode("#242424"));
           g2.fillRect(0, 0, getWidth(), getHeight());
       }
   };
   private JPanel slotPanel =new JPanel(){
       public void paintComponent(Graphics g) {
           Graphics2D g2 = (Graphics2D) g;
           g2.setColor(Color.decode("#242424"));
           g2.fillRect(0, 0, getWidth(), getHeight());
       }
   };
   private JButton save;
    public CarRegistrationFrame() {

        setSize(new Dimension(550,500));
        int x = Toolkit.getDefaultToolkit().getScreenSize().width / 2 - super.getWidth() / 2;
        int y = Toolkit.getDefaultToolkit().getScreenSize().height / 2 - super.getHeight() / 2;
        super.setLocation(x, y);
        setResizable(false);
        mainPanel.setBorder(new EmptyBorder(20,100,20,100));
        mainPanel.setLayout(new FlowLayout(FlowLayout.LEFT,20,20));
        placeName = new JLabel("Place name: ");
        placeName.setFont(new Font("arial",Font.PLAIN,20));
        placeName.setForeground(Color.WHITE);
        mainPanel.add(placeName);

        floorName = new JLabel("Floor name: ");
        floorName.setFont(placeName.getFont());
        floorName.setForeground(placeName.getForeground());
        mainPanel.add(floorName);

        entranceTime = new JLabel("Entrance time: ");
        entranceTime.setForeground(placeName.getForeground());
        entranceTime.setFont(placeName.getFont());
        mainPanel.add(entranceTime);

        platePanel.setLayout(new FlowLayout(FlowLayout.LEFT,10,20));
        plateNumberLabel = new JLabel("Plate number");
        plateNumberLabel.setFont(new Font("arial",Font.PLAIN,18));
        plateNumberLabel.setForeground(placeName.getForeground());
        platePanel.add(plateNumberLabel);
        plateNumber = new JTextField(15);
        plateNumber.setFont(new Font("arial",Font.PLAIN,20));
        platePanel.add(plateNumber);

        mainPanel.add(platePanel);

        slotPanel.setLayout(new FlowLayout(FlowLayout.LEFT,30,10));
        slotCodeLabel = new JLabel("Slot code");
        slotCodeLabel.setForeground(placeName.getForeground());
        slotCodeLabel.setFont(placeName.getFont());
        slotPanel.add(slotCodeLabel);

        slots = new JComboBox();
        slots.setPreferredSize(new Dimension(250,30));
        slots.addItem("Select slots code ");
        slots.setFont(placeName.getFont());
        slots.setForeground(placeName.getForeground());
        slotPanel.add(slots);

        mainPanel.add(slotPanel);

        save = new JButton("Save");
        save.setPreferredSize(new Dimension(400,30));
        mainPanel.add(save);

        save.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

            }
        });
        setContentPane(mainPanel);
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cashiersDashboard.mains;

import java.awt.AlphaComposite;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.geom.AffineTransform;
import javax.swing.JPanel;

/**
 *
 * @author MESERET
 */
public class StartingTabbPane extends JPanel{

    public StartingTabbPane() {
    }
    
    
    @Override
    public void paintComponent(Graphics g) {
        Color c = Color.decode("#242424");
        Graphics2D g2d = (Graphics2D) g;

        g2d.setColor(c);
        g2d.fillRect(0, 0, getWidth(), getHeight());
        g2d.setColor(Color.decode("#5cb85c"));
        g2d.drawRect(0, 0, getWidth() - 1, getHeight() - 1);
        g2d.setColor(Color.gray);
        g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
                RenderingHints.VALUE_ANTIALIAS_ON);

        Dimension d = getSize();
        AffineTransform ct = AffineTransform.getTranslateInstance(d.width / 2,
                d.height * 3 / 4);
        g2d.transform(ct);

        String s = "FreePark parking V1";
        Font f = new Font("nyala", Font.PLAIN, 95);
        g2d.setFont(f);

        int count = 6;
        for (int i = 1; i <= count; i++) {
            AffineTransform oldTransform = g2d.getTransform();

            float ratio = (float) i / (float) count;
            g2d.transform(AffineTransform.getRotateInstance(Math.PI
                    * (ratio - 1.0f)));
            float alpha = ((i == count) ? 1.0f : ratio / 3);
            g2d.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER,
                    alpha));
            g2d.drawString(s, -300, 0);

            g2d.setTransform(oldTransform);

        }
    }

}

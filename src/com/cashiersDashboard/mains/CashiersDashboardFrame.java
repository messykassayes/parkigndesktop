/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cashiersDashboard.mains;

import com.cashiersDashboard.standards.StandardFrame;
import com.models.Cashiers;

import java.awt.Toolkit;

/**
 *
 * @author MESERET
 */
public class CashiersDashboardFrame extends StandardFrame {

    private MainPanel mainPanel;
    public CashiersDashboardFrame() {
        mainPanel = new MainPanel();
        super.addContentPane(mainPanel);
        super.setSize(1500, 750);
        int x = Toolkit.getDefaultToolkit().getScreenSize().width / 2 - super.getWidth() / 2;
        int y = Toolkit.getDefaultToolkit().getScreenSize().height / 2 - super.getHeight() / 2;
        super.setLocation(x, y);
        super.addButtons(true);
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cashiersDashboard.mains;

import com.cashiersDashboard.carDetection.CarDetectionPanel;
import com.cashiersDashboard.standards.*;
import com.constants.ProjectConstants;
import com.google.gson.*;
import com.http.JSONConvertor;
import com.http.MainHttpCall;
import com.models.Cashiers;
import com.models.Floor;
import com.models.StatusFeeder;
import com.properties.PropertyReader;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.SpringLayout;

/**
 *
 * @author MESERET
 */
public class MainPanel extends JPanel {

    private BorderLayout layout;
    private HeaderPanel header;
    private TreePanel tree;
    private WorkBench workbench;
    private StandardLabel file, setting, help, signOut;
    private JMenu changeLanguage;
    private JMenuItem useIPcamera, openVideos, exit, amharic, english, setConnection;
    private CircleButton circle;

    public MainPanel() {
     
        layout = new BorderLayout();
        super.setLayout(layout);
        circle = new CircleButton();
        header = new HeaderPanel(true);
        file = new StandardLabel();
        file.setAddPopUp(true);
        file.setText("File");
        useIPcamera = new JMenuItem("Use IP Camera");
        useIPcamera.setForeground(Color.white);
        openVideos = new JMenuItem("Use Recorded Videos");
        openVideos.setForeground(Color.white);

        exit = new JMenuItem("Exit");
        exit.setForeground(Color.white);
        file.addMenuItem(useIPcamera);
        file.addMenuItem(openVideos);
        file.addMenuItem(exit);

        setting = new StandardLabel();
        setting.setAddPopUp(true);
        setting.setText("Setting");
        changeLanguage = new JMenu("Change Language");
        changeLanguage.setForeground(Color.black);
        amharic = new JMenuItem("አማርኛ");
        amharic.setBackground(Color.black);
        amharic.setForeground(Color.black);
        amharic.setFont(new Font("nyala", Font.PLAIN, 16));
        changeLanguage.add(amharic);
        english = new JMenuItem("English");
        english.setForeground(Color.white);
        changeLanguage.add(english);
        setting.addMenusItems(changeLanguage);

        setConnection = new JMenuItem("Set Your Connection");
        setting.addMenuItem(setConnection);

        help = new StandardLabel();
        help.setText("help");

        header.addMenus(setting, file);
        header.addSingle(help);

        signOut = new StandardLabel();
        signOut.setText("<html><span>&#9660;</span></html>");
        header.addTeEst(signOut);
        circle.userName("M");
        signOut.addSignOutData(circle);
        super.add(header, BorderLayout.NORTH);

        tree = new TreePanel();
        super.add(tree, BorderLayout.WEST);

        workbench = new WorkBench();
        super.add(workbench, BorderLayout.CENTER);
        defaultLangauge();
    }
    public void addVideo(){
    workbench.addVideoPanel(true);
    }
    public void addIpCamera(){
    workbench.addIpCameraPanel(true);
    }
    public void add_data_analysis(){
        workbench.add_data_analysis(true);
    }

    @Override
    public void paintComponent(Graphics g) {
        Graphics2D g2 = (Graphics2D) g;
        g2.setColor(Color.black);
        g2.fillRect(0, 0, getWidth(), getHeight());
    }

    public void defaultLangauge() {
        file.setOriginal(new Font("arial", Font.PLAIN, 14));
        setting.setOriginal(new Font("arial", Font.PLAIN, 14));
        help.setOriginal(new Font("arial", Font.PLAIN, 14));
        signOut.setOriginal(new Font("arial", Font.PLAIN, 14));
    }
    class TreePanel extends JPanel {

    
    private JPanel treeCenterPanel = new JPanel() {
        @Override
        public void paintComponent(Graphics g) {
            Graphics2D g2 = (Graphics2D) g;
            g2.setColor(Color.decode("#242424"));
            g2.fillRect(0, 0, getWidth() - 1, getHeight() - 1);
        }
    };

    private JLabel guloGroups;
    private BorderLayout layout;
    private JLabel choices;
    private StandardButton carDetection, peopleDetection, faceRecognition, carPlateNumber;
    private SpringLayout treeLayout;
    private int buttonClick = 0;
    private JLabel selectionLabel;
    private boolean centerPanelIsTaken = false;
    private String centerPanelIsDoing = null;
    private CircleButton profileImage;
    private StandardLabel2 usersName,show_data_analysis, workignPlaceName;
    private WorkBench workBench;
    private CarDetectionPanel requist;
    private ImageIcon videoIcon;
    private AndroidButton startParking;
    public TreePanel() {
        layout = new BorderLayout();
        super.setLayout(layout);
        super.setPreferredSize(new Dimension(260, 400));
       
        workBench = new WorkBench();
        requist = new CarDetectionPanel();
        //adding components to centerTree panel
        treeLayout = new SpringLayout();
        treeCenterPanel.setLayout(treeLayout);
        
        profileImage = new CircleButton();
        profileImage.userName("P");
        usersName = new StandardLabel2();
        usersName.setFont(new Font("arial",Font.BOLD,20));
        usersName.setForeground(Color.white);
        usersName.setText("Meseret Kassayse");

        // working place name
        workignPlaceName = new StandardLabel2();
        workignPlaceName.setFont(new Font("arial",Font.PLAIN, 16));
        workignPlaceName.setForeground(usersName.getForeground());
        workignPlaceName.setText("How are you today");


        
        show_data_analysis =new StandardLabel2();
        show_data_analysis.setFont(new Font("arial",Font.PLAIN,15));
        show_data_analysis.setForeground(Color.WHITE);
        show_data_analysis.setText("");

        startParking = new AndroidButton(200);
        startParking.setFonts(new Font("arial", Font.PLAIN, 18));
        startParking.setText("Start parking");
        

        treeCenterPanel.add(profileImage);
        treeLayout.putConstraint(SpringLayout.WEST, profileImage, 50, SpringLayout.WEST, treeCenterPanel);
        treeLayout.putConstraint(SpringLayout.NORTH, profileImage, 5, SpringLayout.NORTH, treeCenterPanel);

        treeCenterPanel.add(usersName);
        treeLayout.putConstraint(SpringLayout.WEST, usersName, 30, SpringLayout.WEST, treeCenterPanel);
        treeLayout.putConstraint(SpringLayout.NORTH, usersName, 120, SpringLayout.NORTH, profileImage);

        treeCenterPanel.add(workignPlaceName);
        treeLayout.putConstraint(SpringLayout.WEST, workignPlaceName, 5, SpringLayout.WEST, treeCenterPanel);
        treeLayout.putConstraint(SpringLayout.NORTH, workignPlaceName, 50, SpringLayout.NORTH, usersName);
        

         treeCenterPanel.add(show_data_analysis);
        treeLayout.putConstraint(SpringLayout.WEST, show_data_analysis, 5, SpringLayout.WEST, treeCenterPanel);
        treeLayout.putConstraint(SpringLayout.NORTH, show_data_analysis, 30, SpringLayout.NORTH, workignPlaceName);


        treeCenterPanel.add(startParking);
        treeLayout.putConstraint(SpringLayout.WEST, startParking, 10, SpringLayout.WEST, treeCenterPanel);
        treeLayout.putConstraint(SpringLayout.NORTH, startParking, 50, SpringLayout.NORTH, show_data_analysis);

        
        
        
        guloGroups = new JLabel("FreePark @ ethionet", JLabel.CENTER);
        guloGroups.setFont(new Font("arial", Font.PLAIN, 15));
        guloGroups.setForeground(Color.white);
        guloGroups.setBackground(Color.black);
        super.add(treeCenterPanel, BorderLayout.CENTER);
        super.add(guloGroups, BorderLayout.SOUTH);

         show_data_analysis.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent me){
               add_data_analysis();
          
          }
         });

         startParking.addActionListener(new ActionListener() {
             @Override
             public void actionPerformed(ActionEvent e) {
                 /*MainHttpCall call = new MainHttpCall();
                 StatusFeeder feeder = call.GET(ProjectConstants.API_URL +"no_auth_buildings/"+cashiers.getPlace_id(), PropertyReader.getProperty("token"));
                 JsonObject object = JSONConvertor.convert(feeder.getData());
                 JsonObject object1 = object.getAsJsonObject("data");
                 JsonArray array = object1.getAsJsonArray("floor");
                 List<Floor> floorList = new ArrayList<>();
                 JsonParser parser = new JsonParser();
                 Gson gson = new Gson();
                 for (int i=0;i<array.size();i++){
                     JsonElement element = array.get(i);
                     JsonObject object2 = parser.parse(element.toString()).getAsJsonObject();
                     Floor floor = gson.fromJson(object2, Floor.class);
                     floorList.add(floor);
                 }*/
                 addVideo();
             }
         });
         
        
        defaultLanguage();
       

    }

    public void defaultLanguage() {
    
    }

    public void amharicLanguage() {
        for (StandardButton comp : Arrays.asList(carDetection, peopleDetection, faceRecognition, carPlateNumber)) {
            comp.setFonts(new Font("nyala", Font.BOLD, 16));

        }

        selectionLabel.setFont(new Font("nyala", Font.PLAIN, 16));
        selectionLabel.setText("የትኛውን ነው መስራት የሚፈልጉት");
        carDetection.setText("መኪና መለያ እና መቁጠሪያ");
        peopleDetection.setText("ሰው መለያ እና መቁጠሪያ");
        faceRecognition.setText("ፊት መቆጣጠሪያ");
        carPlateNumber.setText("የመኪና ታርጋ ቁጥር መለያ");
       
        
       

    }


    @Override
    public void paintComponent(Graphics g) {
        Graphics2D g2 = (Graphics2D) g;
        g2.setColor(Color.decode("#242424"));
        g2.fillRect(0, 0, getWidth(), getHeight());
        g2.setColor(Color.black);
        g2.drawRect(0, 0, getWidth() - 1, getHeight() - 1);

    }

}


}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cashiersDashboard.carDetection;

import static com.googlecode.javacv.cpp.opencv_core.cvDrawContours;
import static com.googlecode.javacv.cpp.opencv_imgproc.cvFindContours;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SpringLayout;
import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.MatOfByte;
import org.opencv.core.MatOfPoint;
import org.opencv.core.MatOfRect;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;
import org.opencv.imgproc.Moments;
import org.opencv.objdetect.CascadeClassifier;
import org.opencv.videoio.VideoCapture;

/**
 *
 * @author messy
 */
public class CarsOpencv extends JPanel {

    private JPanel southPanel = new JPanel() {
        @Override
        public void paintComponent(Graphics g) {
            Graphics2D g2 = (Graphics2D) g;
            g2.setColor(Color.decode("#242424"));
            g2.fillRect(0, 0, getWidth(), getHeight());

        }

    };
    Mat mat = new Mat();
    MatOfByte memory = new MatOfByte();
    VideoCapture camera;
    CascadeClassifier cascades;
    MatOfRect rect;
    BufferedImage buff;
    Mythread th;
    Thread thread;
    private double videoLength, frame, fps;
    private String videoName;
    private JLabel carCounter;
    private SpringLayout layout, southLayout;
    private int i = 0;
    private JLabel bigCar, mediumCar, smallCar;
    private JButton bigCarButton, mediumCarButton, smallCarButton;
    private ImageIcon bigIcon, mediumIcon, smallIcon;
    public static final int SENSITIVE_VALUE = 20;
    public static final int BLUR_SIZE = 5;
    private String recorderName, roadName, roadLocation, recordedDate, recordedTime;

    public CarsOpencv() {
        layout = new SpringLayout();
        southLayout = new SpringLayout();
        super.setLayout(layout);
        super.setPreferredSize(new Dimension(640, 580));
       
     
    }

    public void start(String file) {
        camera = new VideoCapture();
        camera.open(file);
        th = new Mythread();
        thread = new Thread(th);
        th.running = true;
        thread.start();

    }

    public void addRoadData(String recorderName, String roadName, String roadLocation, String recordedDate, String recordedTime) {
        this.recorderName = recorderName;
        this.roadName = roadName;
        this.roadLocation = roadLocation;
        this.recordedDate = recordedDate;
        this.recordedTime = recordedTime;
    }

    @Override
    public void paintComponent(Graphics g) {
        Graphics2D g2 = (Graphics2D) g;
        g2.setColor(Color.decode("#242424"));
        g2.fillRect(0, 0, getWidth(), getHeight());

    }

    public void stop() {
        th.running = false;
    }

    public void addComp(CarsTable table, boolean add) {
        super.removeAll();
        super.add(table, BorderLayout.CENTER);
        super.revalidate();
        super.repaint();
        Container container = super.getParent();
        CarDetectionPanel panel = (CarDetectionPanel) container;
        if (add) {
            super.setPreferredSize(new Dimension(730, 480));
        }
        //panel.addComp(this, true);

    }


    class Mythread implements Runnable {

        public boolean running = false;
        int SENSITIVITY_VALUE = 20;
        int BLUR_SIZE = 10;
        int WIDTH = 640;
        int HEIGHT = 480;
        private int carsWidth, carsHeight;

        @Override
        public void run() {

            while (running) {
                if (camera.isOpened()) {
                    try {
                        Mat previusFrame = new Mat();
                        Mat currentFrame = new Mat();
                        Mat grayImage1 = new Mat();
                        Mat grayImage2 = new Mat();
                        Mat abbDifference = new Mat();
                        Mat threshold = new Mat();

                        camera.read(previusFrame);
                        if (!previusFrame.empty()) {
                            Imgproc.resize(previusFrame, previusFrame, new Size(WIDTH, HEIGHT));
                            Imgproc.cvtColor(previusFrame, grayImage1, Imgproc.COLOR_BGR2GRAY);
                            while (running) {
                                boolean success = camera.read(currentFrame);
                                if (success) {
                                    Imgproc.resize(currentFrame, currentFrame, new Size(WIDTH, HEIGHT));
                                    Imgproc.cvtColor(currentFrame, grayImage2, Imgproc.COLOR_BGR2GRAY);
                                } else {
                                    break;
                                }

                                //abbs differnce
                                Core.absdiff(grayImage1, grayImage2, abbDifference);
                                Imgproc.threshold(abbDifference, threshold, SENSITIVITY_VALUE, 255, Imgproc.THRESH_BINARY);
                                Imgproc.blur(threshold, threshold, new Size(BLUR_SIZE, BLUR_SIZE));
                                Imgproc.threshold(threshold, threshold, SENSITIVITY_VALUE, 255, Imgproc.THRESH_BINARY);

                                //finding Movement
                                Mat heirarchy = new Mat();
                                Rect boundingRect = new Rect(0, 0, 0, 0);
                                List<MatOfPoint> contour = new ArrayList<>();
                                Imgproc.findContours(threshold, contour, heirarchy, Imgproc.RETR_EXTERNAL, Imgproc.CHAIN_APPROX_SIMPLE);
                                for (int i = 0; i < contour.size(); i++) {
                                    boundingRect = Imgproc.boundingRect(contour.get(i));

                                    if (boundingRect.width > 50 && boundingRect.height > 50) {
                                        Imgproc.rectangle(currentFrame, boundingRect.tl(), boundingRect.br(), new Scalar(0, 255, 0), 3);
                                        Point center = new Point(boundingRect.x + boundingRect.width / 2, boundingRect.y + boundingRect.height / 2);
                                        Imgproc.circle(currentFrame, center, 3, new Scalar(255, 0, 0), 5, -1, 0);

                                    }

                                }
                                Imgproc.line(currentFrame, new Point(10, 300), new Point(currentFrame.width() - 10, 300), new Scalar(0, 0, 255), 3);

                                Moments moment = Imgproc.moments(threshold);
                                double m10 = moment.get_m10();
                                double m01 = moment.get_m01();
                                double m_area = moment.get_m00();
                                int posx = (int) (m10 / m_area);
                                int posy = (int) (m01 / m_area);
                                if (posy > 202 && posy <= 245) {
                                    i++;
                                }
                                System.err.println(i);
                                MatOfByte matof = new MatOfByte();
                                Imgcodecs.imencode(".bmp", currentFrame, matof);
                                byte[] data = matof.toArray();
                                try {
                                    buff = ImageIO.read(new ByteArrayInputStream(data));
                                } catch (IOException ex) {
                                }
                                Graphics g = getGraphics();

                                g.drawImage(buff, 0, 0, null);

                                grayImage2.copyTo(grayImage1);
                            }

                        } else {
                            CarsTable table = new CarsTable();
                            table.addData(new Object[]{recorderName, roadName, roadLocation, i, recordedTime, "Nurye"});
                            addComp(table, true);
                            break;
                        }
                    } catch (Exception e) {
                    }

                } else {
                    JOptionPane.showMessageDialog(null, "The selected file is not supported by this applications");
                    running = false;
                }

            }
        }

    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cashiersDashboard.carDetection;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GradientPaint;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.geom.RoundRectangle2D;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.plaf.ComponentUI;
import javax.swing.plaf.basic.BasicScrollBarUI;

/**
 *
 * @author messy
 */
public class MyScrollBarUi extends BasicScrollBarUI {

    public static ComponentUI createUI(JComponent c) {
        return new MyScrollBarUi();
    }

    @Override
    protected void paintTrack(Graphics g, JComponent c, Rectangle trackBounds) {
        Graphics2D g2 = (Graphics2D) g;
        g2.setColor(Color.decode("#242424"));
        g2.fillRect((int) trackBounds.getX(), (int) trackBounds.getY(), (int) trackBounds.getWidth(), (int) trackBounds.getHeight());

    }

    @Override
    protected void paintThumb(Graphics g, JComponent c, Rectangle thumbBounds) {
        GradientPaint gradient = new GradientPaint(0, 0, Color.decode("#434343"), 0, (int) thumbBounds.getHeight(), Color.black);
        Graphics2D g2 = (Graphics2D) g;
        g2.setPaint(gradient);
        RoundRectangle2D roundedRectangle = new RoundRectangle2D.Float((int) thumbBounds.getX(), (int) thumbBounds.getY(), (int) thumbBounds.getWidth(), (int) thumbBounds.getHeight(), 10, 10);
        g2.fillRect((int) thumbBounds.getX(), (int) thumbBounds.getY(), (int) thumbBounds.getWidth(), (int) thumbBounds.getHeight());
        g2.draw(roundedRectangle);

    }

    @Override
    protected JButton createDecreaseButton(int orientation) {
        return createZeroButton();
    }

    @Override
    protected JButton createIncreaseButton(int orientation) {
        return createZeroButton();
    }

    private JButton createZeroButton() {
        JButton jbutton = new JButton();
        jbutton.setPreferredSize(new Dimension(0, 0));
        jbutton.setMinimumSize(new Dimension(0, 0));
        jbutton.setMaximumSize(new Dimension(0, 0));
        return jbutton;
    }
}

package com.cashiersDashboard.carDetection;


import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.filechooser.FileNameExtensionFilter;

import com.cashiersDashboard.mains.CarRegistrationFrame;
import com.cashiersDashboard.standards.StandardLabel2;
import com.constants.ProjectConstants;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.http.JSONConvertor;
import com.http.MainHttpCall;
import com.models.Cashiers;
import com.models.Floor;
import com.models.StatusFeeder;
import com.models.TIme;
import com.properties.PropertyReader;
import net.coobird.thumbnailator.Thumbnails;
import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.MatOfByte;
import org.opencv.core.MatOfRect;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;
import org.opencv.objdetect.CascadeClassifier;
import org.opencv.videoio.VideoCapture;

public class ImagePanels extends JPanel {
    private JPanel headerPanel = new JPanel(){
        @Override
        public void paintComponent(Graphics g) {
            Graphics2D g2 = (Graphics2D) g;
            g2.setColor(Color.decode("#5cb85c"));
            g2.fillRect(0, 0, getWidth(), getHeight());
        }
    };

    private JPanel centerPanel = new JPanel(){
        @Override
        public void paintComponent(Graphics g) {
            Graphics2D g2 = (Graphics2D)g;
            g2.setColor(Color.decode("#242424"));
            g2.fillRect(0, 0, getWidth(), getHeight());
            g2.setColor(Color.decode("#5cb85c"));
            g2.drawRect(0,0,getWidth()-1,getHeight()-1);
        }
    };
    private JPanel footerPanel = new JPanel(){
        @Override
        public void paintComponent(Graphics g) {
            Graphics2D g2 = (Graphics2D) g;
            g2.setColor(Color.decode("#242424"));
            g2.fillRect(0, 0, getWidth()-1, getHeight()-1);
        }
    };
    private JPanel footerBtnPanel = new JPanel(){
        @Override
        public void paintComponent(Graphics g) {
            Graphics2D g2 = (Graphics2D) g;
            g2.setColor(Color.decode("#242424"));
            g2.fillRect(0, 0, getWidth()-1, getHeight()-1);
        }
    };


    private JPanel opencvPanel = new JPanel(){
        @Override
        public void paintComponent(Graphics g) {
            Graphics2D g2 = (Graphics2D) g;
            g2.setColor(Color.decode("#242424"));
            g2.fillRect(0, 0, getWidth()-1, getHeight()-1);
        }
    };
    private JLabel floorName,slotLabel;
    private JButton enterCar,carExit,test;
    private JFileChooser fileChooser;
    private FileNameExtensionFilter filter;
    //private VideoCapture camera;
    private Thread thread;
    //private MyThread myThread;
    private boolean start = true;
    public ImagePanels(){

//        camera = new VideoCapture();
        setLayout(new BorderLayout());
        setPreferredSize(new Dimension(1000,600));
        headerPanel.setLayout(new BorderLayout());
        headerPanel.setBorder(new EmptyBorder(10,10,10,10));

        floorName = new JLabel("Entrance");
        floorName.setFont(new Font("arial", Font.PLAIN, 25));
        floorName.setForeground(Color.WHITE);
        headerPanel.add(floorName, BorderLayout.WEST);

        headerPanel.add(Box.createHorizontalStrut((int) this.getPreferredSize().getWidth()-50));

        slotLabel = new JLabel();
        slotLabel.setForeground(Color.decode("#242424"));
        slotLabel.setFont(new Font("arial",Font.PLAIN,18));
        slotLabel.setText("Slots: ");
        headerPanel.add(slotLabel, BorderLayout.EAST);



        setBorder(new EmptyBorder(10,20,10,20));
        add(headerPanel, BorderLayout.NORTH);

        //adding item to center panel
        footerPanel.setLayout(new BorderLayout());
        footerBtnPanel.setLayout(new FlowLayout(FlowLayout.RIGHT,20,20));

        enterCar = new JButton("Add car");
        carExit = new JButton("payment");
        test = new JButton("Test");
        footerBtnPanel.add(enterCar);
        footerBtnPanel.add(carExit);
        footerBtnPanel.add(test);
        footerPanel.add(footerBtnPanel,BorderLayout.EAST);

        centerPanel.setLayout(new BorderLayout(10,10));

       centerPanel.add(opencvPanel,BorderLayout.CENTER);


        centerPanel.add(footerPanel,BorderLayout.SOUTH);

        add(centerPanel, BorderLayout.CENTER);

        enterCar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                MainHttpCall call = new MainHttpCall();
                Gson gson = new Gson();
                StatusFeeder feeder = call.GET(ProjectConstants.API_URL+"times", PropertyReader.getProperty("token"));
                JsonObject object = JSONConvertor.convert(feeder.getData());
                TIme tIme = gson.fromJson(object.toString(),TIme.class);
                CarRegistrationFrame registrationFrame =new CarRegistrationFrame();
                registrationFrame.setVisible(true);
            }
        });

        fileChooser = new JFileChooser();
        fileChooser.setDialogTitle("Select File");
        filter = new FileNameExtensionFilter("all video file", "avi", "mp4","mkv");
        fileChooser.setFileFilter(filter);
        carExit.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                //startOpencv("");
            }
        });
        test.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int videoFiles = fileChooser.showOpenDialog(null);
                if (videoFiles == JFileChooser.APPROVE_OPTION) {
                    File file = fileChooser.getSelectedFile();
                   String selectedFileName = file.getAbsolutePath();
                    startOpencv(selectedFileName);
                }
            }
        });


    }

    public void startOpencv(String fileType){
        /*myThread = new MyThread(fileType);
        thread = new Thread(myThread);
        thread.start();*/
    }

    public static BufferedImage Mat2bufferedImage(Mat image) {
        MatOfByte bytemat = new MatOfByte();
        Imgcodecs.imencode(".bmp", image, bytemat);
        byte[] bytes = bytemat.toArray();
        InputStream in = new ByteArrayInputStream(bytes);
        BufferedImage img = null;
        try {
            img = ImageIO.read(in);
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return img;
    }

    public static Mat detectCars(Mat image){


        CascadeClassifier cardetector = new CascadeClassifier("/home/meseret/vision/vehicle_detection_haarcascades/cars.xml");



        MatOfRect faceDetections = new MatOfRect();
        cardetector.detectMultiScale(image, faceDetections);

       // System.out.println(String.format("Detected %s cars", faceDetections.toArray().length));

        for (Rect rect : faceDetections.toArray()) {
            Imgproc.rectangle(image, new Point(rect.x, rect.y), new Point(rect.x + rect.width, rect.y + rect.height),
                    new Scalar(0, 255, 0),2);
        }

        /* String filename = "C:\\Users\\User\\Desktop\\Road and Cars\\Outputs\\";
         System.out.println(String.format("Writing %s", filename));*/
        return image;
    }

    /*class MyThread implements Runnable{
        private String fileName;

        public MyThread(String fileName) {
            this.fileName = fileName;
        }

        @Override
        public void run() {
            Mat frame = new Mat();
            camera.open(fileName);
            boolean read = camera.read(frame);
            System.out.println(read);
            while (true) {
                if (camera.read(frame)) {
                    try{
                        frame=detectCars(frame);
                        BufferedImage image = Mat2bufferedImage(frame);
                        BufferedImage images = Thumbnails.of(image).width(opencvPanel.getWidth()-20).asBufferedImage();
                        Graphics graphics = opencvPanel.getGraphics();
                        graphics.drawImage(images,10,5,null);
                    }catch (Exception e){

                    }
                }
            }
        }
    }*/

}

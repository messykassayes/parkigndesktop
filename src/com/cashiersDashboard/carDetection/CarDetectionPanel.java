/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cashiersDashboard.carDetection;


import com.models.Cashiers;
import com.models.Floor;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.util.List;

/**
 * #5cb85c
 *
 * @author messy
 */
public class CarDetectionPanel extends JPanel {
    private List<Floor> floorList;
    public static final int N =80;
    private JScrollPane scrollPane;
    private JPanel mainPanel = new JPanel(){
        @Override
        public void paintComponent(Graphics g) {
            Graphics2D g2 = (Graphics2D) g;
            g2.setColor(Color.decode("#242424"));
            g2.fillRect(0, 0, getWidth(), getHeight());
        }
    };
    private SpringLayout layout;
    public CarDetectionPanel() {
        setLayout(new BorderLayout(20,20));

        layout = new SpringLayout();
        mainPanel.setLayout(new BoxLayout(mainPanel,BoxLayout.Y_AXIS));

        scrollPane = new JScrollPane(mainPanel, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        scrollPane.getViewport().setMinimumSize(new Dimension(160,200));
        scrollPane.getViewport().setPreferredSize(new Dimension(this.getWidth(),this.getHeight()));
        add(scrollPane, BorderLayout.CENTER);
        setFloors();
    }

    @Override
    public void paintComponent(Graphics g) {
        Graphics2D g2 = (Graphics2D) g;
        g2.setColor(Color.decode("#242424"));
        g2.fillRect(0, 0, getWidth(), getHeight());
    }

    public void setFloors(){
        for (int i=0;i<2;i++) {

            ImagePanels panels = new ImagePanels();
            mainPanel.add(panels);
            mainPanel.add(Box.createHorizontalStrut(N));
        }

    }
}
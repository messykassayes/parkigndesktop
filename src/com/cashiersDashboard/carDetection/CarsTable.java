/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cashiersDashboard.carDetection;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.geom.RoundRectangle2D;
import java.util.Arrays;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.RowFilter;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableColumn;
import javax.swing.table.TableRowSorter;

/**
 *
 * @author messy
 */
public class CarsTable extends JPanel {

    private JScrollPane pane = new JScrollPane() {
        @Override
        public void paintComponent(Graphics g) {
            Graphics2D g2 = (Graphics2D) g;
            g2.setColor(Color.decode("#242424"));
            g2.fillRect(0, 0, getWidth() - 1, getHeight() - 1);

        }
    };
    private JPanel northPanel = new JPanel() {
        @Override
        public void paintComponent(Graphics g) {

            Graphics2D g2 = (Graphics2D) g;
            g2.setColor(Color.decode("#242424"));
            g2.fillRect(0, 0, getWidth(), getHeight());

        }
    };
    private JPanel buttonPane = new JPanel() {
        @Override
        public void paintComponent(Graphics g) {

            Graphics2D g2 = (Graphics2D) g;
            g2.setColor(Color.decode("#242424"));
            g2.fillRect(0, 0, getWidth(), getHeight());

        }
    };
    private MyTableHeaders header;
    private String[] columnNames = {"የመንገዱ ስም", "መንገዱ የሚገኝበት ቦታ", "የተቀረጸበት ሰአት", "ትልቅ መኪና", "መካከለኛ መኪና", "አነስተኛ መኪና"};
    private String[][] data = {{"", "", "", "", "", ""}};
    private DefaultTableModel tableModel = new DefaultTableModel() {
        @Override
        public boolean isCellEditable(int row, int column) {
            return false;
        }

    };
    private JTable table = new JTable();
    private SearchTextfield search;
    private ImageIcon icon;
    private TableRowSorter sorter;
    private DefaultTableModel sortModel;
    private ToolBarButton docButton, printButton, sortbutton, showAllData;
    private ImageIcon docIcon, printIcon, sortIcon, showIcon;
    private JLabel hintLabel;
    private String hintText;

    public CarsTable() {
        super.setLayout(new BorderLayout());
        //adding toolbar button
        buttonPane.setLayout(new FlowLayout(FlowLayout.LEFT, 10, 0));
        sortbutton = new ToolBarButton();
        sortIcon = new ImageIcon(this.getClass().getResource("/images/sort.png"));
        ImageIcon sortIcon2 = new ImageIcon(this.getClass().getResource("/images/sort2.png"));
        sortbutton.setIcon(sortIcon);
        sortbutton.setActionCommand("sort");
        sortbutton.setRolloverIcon(sortIcon2);
        buttonPane.add(sortbutton);

        printButton = new ToolBarButton();
        printIcon = new ImageIcon(this.getClass().getResource("/images/print.png"));
        ImageIcon printIcon2 = new ImageIcon(this.getClass().getResource("/images/print2.png"));
        printButton.setIcon(printIcon);
        printButton.setActionCommand("print");
        printButton.setRolloverIcon(printIcon2);
        buttonPane.add(printButton);

        docButton = new ToolBarButton();
        docIcon = new ImageIcon(this.getClass().getResource("/images/doc.png"));
        ImageIcon docIcon2 = new ImageIcon(this.getClass().getResource("/images/doc2.png"));
        docButton.setIcon(docIcon);
        docButton.setActionCommand("save");
        docButton.setRolloverIcon(docIcon2);
        buttonPane.add(docButton);

        showAllData = new ToolBarButton();
        showIcon = new ImageIcon(this.getClass().getResource("/images/doc.png"));
        ImageIcon showIcon2 = new ImageIcon(this.getClass().getResource("/images/doc2.png"));
        showAllData.setIcon(showIcon);
        showAllData.setActionCommand("showAllData");
        showAllData.setRolloverIcon(showIcon2);
        buttonPane.add(showAllData);

        hintLabel = new JLabel();
        hintLabel.setForeground(Color.white);
        buttonPane.add(hintLabel);
        //adding components to north panel
        northPanel.setLayout(new BorderLayout());
        icon = new ImageIcon(this.getClass().getResource("/images/index.png"));
        search = new SearchTextfield(20);
        search.setPlaceholder("Search by road name");
        search.setIcon(icon);
        northPanel.add(search, BorderLayout.EAST);
        northPanel.add(buttonPane, BorderLayout.WEST);
        header = new MyTableHeaders();
        table.getTableHeader().setDefaultRenderer(header);
        JTableHeader header = table.getTableHeader();
        header.setForeground(Color.white);
        header.setBackground(Color.decode("#242424"));

        table.setBorder(BorderFactory.createEmptyBorder());
        table.setDefaultRenderer(Object.class, new myCellRenderer());
        tableModel.setColumnIdentifiers(columnNames);
        table.setIntercellSpacing(new Dimension(0, 0));
        table.setRowHeight(20);
        table.setRowSelectionAllowed(true);
        table.setModel(tableModel);
        table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        table.setShowGrid(false);
        table.setSurrendersFocusOnKeystroke(true);
        pane.setViewportView(table);
        pane.setPreferredSize(new Dimension(730, 480));
        pane.getViewport().setBackground(Color.decode("#242424"));

        for (int i = 0;
                i < table.getColumnCount();
                i++) {
            TableColumn tableColumn = table.getColumnModel().getColumn(i);

            if (i == 1) {
                tableColumn.setPreferredWidth(167);
            } else if (i == 4 || i == 2) {
                tableColumn.setPreferredWidth(124);
            } else {
                tableColumn.setPreferredWidth(105);
            }

        }

        sortModel = (DefaultTableModel) table.getModel();
        sorter = new TableRowSorter<DefaultTableModel>(sortModel);
        table.setRowSorter(sorter);
        search.addKeyListener(new KeyListener() {

            @Override
            public void keyTyped(KeyEvent e) {
            }

            @Override
            public void keyPressed(KeyEvent e) {
            }

            @Override
            public void keyReleased(KeyEvent e) {
                String text = search.getText();
                sorter.setRowFilter(RowFilter.regexFilter(text));
            }
        });

        pane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
        pane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        pane.getVerticalScrollBar().setUI(new MyScrollBarUi());
        pane.getHorizontalScrollBar().setUI(new MyScrollBarUi());
        pane.setBorder(BorderFactory.createEmptyBorder());
        northPanel.setPreferredSize(new Dimension(200, 32));
        //super.setBorder(BorderFactory.createEtchedBorder(Color.gray, Color.gray));
        super.setPreferredSize(new Dimension(730, 480));
        super.add(northPanel, BorderLayout.NORTH);
        super.add(pane, BorderLayout.CENTER);
        defaultLanguage();

    }

    public String getHintText() {
        return hintText;
    }

    public void setHintText(String text) {
        switch (text) {
            case "print":
                System.err.println("Print");
                hintLabel.setText("Print this File");

                break;
            case "save":
                hintLabel.setText("Save this File as microsoft word Document");
                break;
            case "sort":
                hintLabel.setText("Sort as Ascending or Descending order");
                break;
            case "showAllData":
                hintLabel.setText("Show All Roads Data");
                break;
            case "null":
                hintLabel.setText("");
                break;

        }
    }

    public void addData(Object[] data) {
        tableModel.addRow(data);
    }

    public void defaultLanguage() {
        for (JComponent comp : Arrays.asList(hintLabel)) {
            comp.setFont(new Font("arial", Font.PLAIN, 14));
        }
    }

    @Override

    public void paintComponent(Graphics g) {
        Graphics2D g2 = (Graphics2D) g;
        g2.setColor(Color.black);
        g2.fillRect(0, 0, getWidth() - 1, getHeight() - 1);

    }

    class myCellRenderer extends DefaultTableCellRenderer {

        @Override
        public Component getTableCellRendererComponent(JTable table,
                Object value, boolean isSelected, boolean hasFocus,
                int row, int column) {
            JComponent c = (JComponent) super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
            JLabel comp = (JLabel) c;
            comp.setFont(new Font("arial", Font.PLAIN, 15));
            comp.setForeground(Color.white);
            comp.setBackground(Color.decode("#242424"));
            comp.setBorder(BorderFactory.createEmptyBorder());
            if (isSelected) {
                comp.setBackground(Color.decode("#8499c6"));
                if (column == 0 && column == table.getColumnCount()) {
                    Graphics g = c.getGraphics();
                    Graphics2D g2 = (Graphics2D) g;
                    RoundRectangle2D roundedRectangle = new RoundRectangle2D.Float(0, 0, c.getWidth(), c.getHeight(), 10, 10);
                    g2.draw(roundedRectangle);
                }

            }
            return comp;
        }
    }

    class ToolBarButton extends JButton {

        private Ml ml;

        public ToolBarButton() {
            ml = new Ml(this);
            super.addMouseListener(ml);
            super.addMouseMotionListener(ml);
            super.setFocusPainted(false);
            super.setBorderPainted(false);
            super.setContentAreaFilled(false);
            super.setBorder(BorderFactory.createEmptyBorder());
            super.setBorder(BorderFactory.createEtchedBorder(Color.black, Color.black));

        }

        class Ml extends MouseAdapter {

            private ToolBarButton button;

            public Ml(ToolBarButton buttn) {
                this.button = buttn;
            }

            @Override
            public void mouseEntered(MouseEvent e) {
                JButton b = (JButton) e.getSource();
                String text = b.getActionCommand();
                setHintText(text);

            }

            @Override
            public void mouseExited(MouseEvent e) {
                JButton b = (JButton) e.getSource();
                setHintText("null");
            }

        }

    }

}

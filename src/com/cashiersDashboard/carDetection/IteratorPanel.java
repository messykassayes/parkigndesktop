/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cashiersDashboard.carDetection;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import javax.imageio.ImageIO;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SpringLayout;
import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.MatOfByte;
import org.opencv.core.MatOfPoint;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;
import org.opencv.imgproc.Moments;
import org.opencv.videoio.VideoCapture;

/**
 *
 * @author messy
 */
public class IteratorPanel extends JPanel {

    private Iterator it;
    private MyThread thread;
    private Thread th;
    private VideoCapture camera;
    MatOfByte memory = new MatOfByte();
    Mat previusFrame = new Mat();
    Mat currentFrame = new Mat();
    Mat grayImage1 = new Mat();
    Mat grayImage2 = new Mat();
    Mat abbDifference = new Mat();
    Mat thresholdImage = new Mat();
    private String firstFileName, fileName;
    private ArrayList list;
    private int i;
    private BufferedImage buff;
    private JLabel small_cars,medium_cars,large_cars;
    private JPanel center_panel=new JPanel(){
         @Override
    public void paintComponent(Graphics g) {
        Graphics2D g2 = (Graphics2D) g;
        g2.setColor(Color.decode("#242424"));
        g2.fillRect(0, 0, getWidth(), getHeight());

    } 
    };
    private JPanel east_panel=new JPanel(){
         @Override
    public void paintComponent(Graphics g) {
        Graphics2D g2 = (Graphics2D) g;
        g2.setColor(Color.decode("#242424"));
        g2.fillRect(0, 0, getWidth(), getHeight());

    } 
    };
    private SpringLayout east_panel_layout,main_layout;
    private int small_car_no=0,medium_car=0,large_car=0;
    public IteratorPanel() {
       
        list = new ArrayList();
        super.setPreferredSize(new Dimension(3000, 480));
        center_panel.setPreferredSize(new Dimension(640, 480));
        east_panel.setPreferredSize(new Dimension(300, 480));
        
        
        
        east_panel_layout=new SpringLayout();
        main_layout = new SpringLayout();
        super.setLayout(main_layout);
        east_panel.setLayout(east_panel_layout);
        small_cars = new JLabel();
        small_cars.setForeground(Color.white);
        small_cars.setFont(new Font("arial",Font.BOLD,25));
        
        medium_cars = new JLabel("Medium Cars: "+medium_car);
        medium_cars.setForeground(Color.white);
        medium_cars.setFont(new Font("arial",Font.BOLD,25));
        
        large_cars = new JLabel("Large Cars: "+large_car);
        large_cars.setForeground(Color.white);
        large_cars.setFont(new Font("arial",Font.BOLD,25));
        
        east_panel.add(small_cars);
        east_panel_layout.putConstraint(SpringLayout.WEST, small_cars, 20, SpringLayout.WEST, east_panel);
        east_panel_layout.putConstraint(SpringLayout.NORTH, small_cars, 50, SpringLayout.NORTH, east_panel);
         
        east_panel.add(medium_cars);
        east_panel_layout.putConstraint(SpringLayout.WEST, medium_cars, 20, SpringLayout.WEST, east_panel);
        east_panel_layout.putConstraint(SpringLayout.NORTH, medium_cars, 100, SpringLayout.NORTH, small_cars);
         
        east_panel.add(large_cars);
        east_panel_layout.putConstraint(SpringLayout.WEST, large_cars, 20, SpringLayout.WEST, east_panel);
        east_panel_layout.putConstraint(SpringLayout.NORTH, large_cars, 100, SpringLayout.NORTH, medium_cars);
        
        super.add(center_panel);
        main_layout.putConstraint(SpringLayout.WEST, center_panel, 10, SpringLayout.WEST, this);
        main_layout.putConstraint(SpringLayout.NORTH, center_panel, 10, SpringLayout.NORTH, this);
         
         super.add(east_panel);
        main_layout.putConstraint(SpringLayout.WEST, east_panel, 650, SpringLayout.WEST, center_panel);
        main_layout.putConstraint(SpringLayout.NORTH, east_panel, 10, SpringLayout.NORTH, this);
         
        
    }

    public void AddFiles(Iterator its) {
        this.it = its;
        camera = new VideoCapture();

        firstFileName = it.next().toString();
        camera.open(firstFileName);
        System.err.println("FirstFile " + firstFileName);
        thread = new MyThread();
        th = new Thread(thread);
        thread.running = true;
        th.start();

    }

    @Override
    public void paintComponent(Graphics g) {
        Graphics2D g2 = (Graphics2D) g;
        g2.setColor(Color.decode("#242424"));
        g2.fillRect(0, 0, getWidth(), getHeight());

    }
    public void final_result(int smallcar,int mediumcar,int largecar){
        super.removeAll();
       
        CarsTable table = new CarsTable();
        table.addData(new Object[]{"Rwanda", "gotera", "2:30", largecar, mediumcar, smallcar});
        super.add(table);
        main_layout.putConstraint(SpringLayout.WEST, table, 10, SpringLayout.WEST, this);
        main_layout.putConstraint(SpringLayout.NORTH, table, 10, SpringLayout.NORTH, this);
        super.repaint();
        super.revalidate();
       
                     
    }

    class MyThread implements Runnable {

        private boolean running = false;
        int SENSITIVITY_VALUE = 20;
        int BLUR_SIZE = 10;
        int WIDTH = 640;
        int HEIGHT = 480;

        @Override
        public void run() {
            int i = 0;
            while (running) {
                if (camera.isOpened()) {
                    try {
                        camera.read(previusFrame);
                        if (!previusFrame.empty()) {
                            Imgproc.resize(previusFrame, previusFrame, new Size(640, 480));
                            Imgproc.cvtColor(previusFrame, grayImage1, Imgproc.COLOR_BGR2GRAY);

                            while (running) {
                                boolean success = camera.read(currentFrame);
                                if (success) {
                                    Imgproc.resize(currentFrame, currentFrame, new Size(640, 480));
                                    Imgproc.resize(currentFrame, currentFrame, new Size(WIDTH, HEIGHT));
                                    Imgproc.cvtColor(currentFrame, grayImage2, Imgproc.COLOR_BGR2GRAY);

                                } else {
                                    break;
                                }
                                //thresholding and differerntiating stars here
                                Core.absdiff(grayImage1, grayImage2, abbDifference);
                                Imgproc.threshold(abbDifference, thresholdImage, SENSITIVITY_VALUE, 255, Imgproc.THRESH_BINARY);
                                Imgproc.blur(thresholdImage, thresholdImage, new Size(BLUR_SIZE, BLUR_SIZE));
                                Imgproc.threshold(thresholdImage, thresholdImage, SENSITIVITY_VALUE, 255, Imgproc.THRESH_BINARY);

                                //finding Movement
                                Mat heirarchy = new Mat();
                                Rect boundingRect = new Rect(0, 0, 0, 0);
                                List<MatOfPoint> contour = new ArrayList<MatOfPoint>();
                                Imgproc.findContours(thresholdImage, contour, heirarchy, Imgproc.RETR_EXTERNAL, Imgproc.CHAIN_APPROX_SIMPLE);
                                for (int j = 0; j < contour.size(); j++) {
                                    boundingRect = Imgproc.boundingRect(contour.get(j));

                                    if (boundingRect.width > 30 && boundingRect.height > 30 && boundingRect.width < 200) {
                                        Imgproc.rectangle(currentFrame, boundingRect.tl(), boundingRect.br(), new Scalar(0, 255, 0), 3);
                                        Point center = new Point(boundingRect.x + boundingRect.width / 2, boundingRect.y + boundingRect.height / 2);
                                        Imgproc.circle(currentFrame, center, 3, new Scalar(255, 0, 0), 5, -1, 0);
                                    }
                          
                   
                                }

                                Imgproc.line(currentFrame, new Point(10, 300), new Point(currentFrame.width() - 10, 300), new Scalar(0, 0, 255), 3);

                                Moments moment = Imgproc.moments(thresholdImage);
                                double m10 = moment.get_m10();
                                double m01 = moment.get_m01();
                                double m_area = moment.get_m00();
                                int posx = (int) (m10 / m_area);
                                int posy = (int) (m01 / m_area);
                                if (posy > 202 && posy <= 245) {
                                    i++;
                                    small_car_no++;
                                }
                                //optical flow
                                small_cars.setText("Small Cars: "+i);
                                 System.out.println(i);
                                Imgcodecs.imencode(".bmp", currentFrame, memory);
                                byte[] data = memory.toArray();
                                buff = ImageIO.read(new ByteArrayInputStream(data));
                                Graphics g = center_panel.getGraphics();
                                g.drawImage(buff, 0, 0, null);

                                grayImage2.copyTo(grayImage1);
                            }
                            ///database goes here
                            if (!firstFileName.equals("")) {
                                System.err.println(firstFileName);
                                System.err.println(i);
                            }
                            if (!fileName.equals("")) {
                                System.err.println(fileName);
                                System.err.println(i);
                                String roadName = fileName.substring(0, fileName.indexOf("/"));
                                System.err.println("roadName " + roadName);
                            }

                            //iterator next file opened here
                        } else {
                            if (it.hasNext()) {
                                fileName = it.next().toString();
                                camera.open(fileName);
                                firstFileName = "";
                                i = 0;

                            } else {
                                System.err.println("File Completed");
                                final_result(small_car_no,medium_car,large_car);
                             break;
                            }
                        }
                    } catch (Exception e) {
                    }
                }
            }

        }

    }

}

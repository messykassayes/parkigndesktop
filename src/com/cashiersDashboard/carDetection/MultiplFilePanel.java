/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cashiersDashboard.carDetection;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SpringLayout;
import javax.swing.filechooser.FileNameExtensionFilter;

/**
 *
 * @author messy
 */
public class MultiplFilePanel extends JPanel {

    private JLabel selecteFile;
    private JFileChooser chooser;
    private SpringLayout layout;
    private JButton selectButton;
    private ImageIcon icon1, icon2;
    private JLabel information;
    private FileNameExtensionFilter filter;
    private List fileName;
   private JLabel back;
  
    public MultiplFilePanel() {
        
       
        layout = new SpringLayout();
        super.setLayout(layout);
        super.setPreferredSize(new Dimension(2000, 600));
        fileName = new ArrayList();
        chooser = new JFileChooser();
        filter = new FileNameExtensionFilter("all file", "avi", "mp4");
        chooser.setFileFilter(filter);
        chooser.setMultiSelectionEnabled(true);
        information = new JLabel();
        information.setForeground(Color.white);
        selecteFile = new JLabel();
        selecteFile.setForeground(Color.white);

        selectButton = new JButton();
        icon1 = new ImageIcon(this.getClass().getResource("/com/images/select.png"));
        icon2 = new ImageIcon(this.getClass().getResource("/com/images/select2.png"));
        selectButton.setIcon(icon1);
        selectButton.setRolloverIcon(icon2);
        selectButton.setFocusPainted(false);
        selectButton.setContentAreaFilled(false);
        selectButton.setBorderPainted(false);
        
        back= new JLabel("<html>&larr;</html>");
        back.setFont(new Font("arial",Font.BOLD,20));
        back.setForeground(Color.white);
        back.setToolTipText("Back to Previus");

        super.add(back);
        layout.putConstraint(SpringLayout.WEST, back, 2, SpringLayout.WEST, this);
        layout.putConstraint(SpringLayout.NORTH, back, 5, SpringLayout.NORTH, this);

        
        
        super.add(information);
        layout.putConstraint(SpringLayout.WEST, information, 100, SpringLayout.WEST, this);
        layout.putConstraint(SpringLayout.NORTH, information, 30, SpringLayout.NORTH, back);

        super.add(selecteFile);
        layout.putConstraint(SpringLayout.WEST, selecteFile, 60, SpringLayout.WEST, this);
        layout.putConstraint(SpringLayout.NORTH, selecteFile, 50, SpringLayout.NORTH, information);

        super.add(selectButton);
        layout.putConstraint(SpringLayout.WEST, selectButton, 100, SpringLayout.WEST, selecteFile);
        layout.putConstraint(SpringLayout.NORTH, selectButton, 40, SpringLayout.NORTH, information);
        selectButton.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                int result = chooser.showOpenDialog(null);
                if (result == JFileChooser.APPROVE_OPTION) {
                    File[] files = chooser.getSelectedFiles();
                    for (int i = 0; i < files.length; i++) {
                        fileName.add(files[i]);
                    }
                    Iterator it = fileName.iterator();
                    IteratorPanel itPanel = new IteratorPanel();
                    itPanel.AddFiles(it);
                    addComp(itPanel);

                }
            }
        });
        defaultLanguage();
        back.addMouseListener(new MouseAdapter(){});
    }

    public void addComp(IteratorPanel panel) {
        super.removeAll();
        super.add(panel);
        layout.putConstraint(SpringLayout.WEST, panel, 2, SpringLayout.WEST, this);
        layout.putConstraint(SpringLayout.NORTH, panel, 5, SpringLayout.NORTH, this);
        super.repaint();
        super.revalidate();
    }

    public void defaultLanguage() {

        information.setText("Select your multiple files");
        information.setFont(new Font("arial", Font.PLAIN, 17));
        selecteFile.setText("Add Your files");
        selecteFile.setFont(new Font("arial", Font.PLAIN, 14));

    }

    @Override
    public void paintComponent(Graphics g) {
        Graphics2D g2 = (Graphics2D) g;
        g2.setColor(Color.decode("#242424"));
        g2.fillRect(0, 0, getWidth(), getHeight());

    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cashiersDashboard.carDetection;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import javax.swing.JPanel;

/**
 *
 * @author MESERET
 */
public class IpCameraPanel extends JPanel{

    public IpCameraPanel() {
    }
    
    
     @Override
    public void paintComponent(Graphics g) {
        Graphics2D g2 = (Graphics2D) g;
        g2.setColor(Color.decode("#242424"));
        g2.fillRect(0, 0, getWidth(), getHeight());
        g2.setColor(Color.decode("#5cb85c"));
        g2.drawRect(0, 0, getWidth() - 1, getHeight() - 1);

    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cashiersDashboard.carDetection;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Insets;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import javax.swing.BorderFactory;
import javax.swing.FocusManager;
import javax.swing.Icon;
import javax.swing.JTextField;
import javax.swing.UIManager;
import javax.swing.border.Border;

/**
 *
 * @author messy
 */
public class SearchTextfield extends JTextField {

    private placeHolder holder;
    private String placeholder;
    private Font fonts;
    private Border border;
    private JTextField field;
    private Insets inset;
    private Icon icon;

    public SearchTextfield(int column) {
        super.setColumns(column);
        super.setPreferredSize(new Dimension(10, 30));
        super.setBackground(Color.decode("#2D2D2D"));
        super.setBorder(BorderFactory.createEtchedBorder(Color.decode("#2D2D2D"), Color.decode("#2D2D2D")));
        super.setForeground(Color.white);
        holder = new placeHolder(this);

        border = UIManager.getBorder("TextField.border");
        field = new JTextField();
        inset = border.getBorderInsets(field);
    }

    public Font getFonts() {
        return fonts;
    }

    public void setFonts(Font fonts) {
        this.fonts = fonts;
    }

    public String getPlaceholder() {
        return placeholder;
    }

    public Icon getIcon() {
        return icon;
    }

    public void setIcon(Icon icon) {
        this.icon = icon;
    }

    public void setPlaceholder(String placeholder) {
        this.placeholder = placeholder;
    }

    @Override
    public void paintComponent(Graphics g) {
       super.paintComponent(g);
        int x, y;
        int textx = 2;
        if (this.icon != null) {
            int iconWidth = icon.getIconWidth();
            int iconHeight = icon.getIconHeight();
            x = inset.right +125;
            textx = iconWidth + 5;
            y = (this.getHeight() - iconHeight) / 2;
            icon.paintIcon(this, g, x, y);
        }
        setMargin(new Insets(2, textx, 2, 2));
    }

    @Override
    public void paint(Graphics g) {
        super.paint(g);
        holder.paint(g);

    }

    class placeHolder implements FocusListener {

        private SearchTextfield field;

        public placeHolder(SearchTextfield fi) {
            this.field = fi;
            field.addFocusListener(this);

        }

        @Override
        public void focusGained(FocusEvent e) {
            field.repaint();
        }

        @Override
        public void focusLost(FocusEvent e) {
            field.repaint();
        }

        public void paint(Graphics g) {
            Graphics2D g2 = (Graphics2D) g.create();
            if (field.getText().isEmpty() && (FocusManager.getCurrentKeyboardFocusManager().getFocusOwner() != field)) {

                g2.setFont(field.getFonts());
                g2.setColor(Color.gray);
                g2.drawString(field.getPlaceholder(), 10, 18);
                field.setBorder(BorderFactory.createEtchedBorder(Color.black, Color.black));

            }
        }
    }
}

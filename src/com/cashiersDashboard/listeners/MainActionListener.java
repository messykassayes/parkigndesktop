/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cashiersDashboard.listeners;

import java.awt.Frame;
import java.awt.GraphicsConfiguration;
import java.awt.Rectangle;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;
import sun.java2d.SunGraphicsEnvironment;

/**
 *
 * @author messy
 */
public class MainActionListener implements ActionListener {

    @Override
    public void actionPerformed(ActionEvent e) {
        String actionCommand = e.getActionCommand();
        switch (actionCommand) {
            case "minimize":
                Object mimimize = e.getSource();
                JComponent comp = (JComponent) mimimize;
                Window w = SwingUtilities.getWindowAncestor(comp);
                JFrame frame = (JFrame) w;
                frame.setState(Frame.ICONIFIED);
                break;
            case "maximize":
                Object maximize = e.getSource();
                JComponent comps = (JComponent) maximize;
                Window ws = SwingUtilities.getWindowAncestor(comps);
                JFrame frames = (JFrame) ws;
                if (frames.getExtendedState() == Frame.MAXIMIZED_BOTH) {
                    frames.setExtendedState(Frame.NORMAL);
                    comps.setToolTipText("Maximize");
                } else {
                    GraphicsConfiguration config = frames.getGraphicsConfiguration();
                    Rectangle rectangle = SunGraphicsEnvironment.getUsableBounds(config.getDevice());
                    frames.setMaximizedBounds(rectangle);
                    frames.setExtendedState(Frame.MAXIMIZED_BOTH);
                    comps.setToolTipText("Restore Down");
                }
                break;
            case "close":
                Object close = e.getSource();
                JComponent comp3 = (JComponent) close;
                Window w3 = SwingUtilities.getWindowAncestor(comp3);
                JFrame frame3 = (JFrame) w3;
                int checkUp = JOptionPane.showConfirmDialog(null, "Are you sure you want to exit", "Conformation", JOptionPane.YES_NO_OPTION);
                if (checkUp == JOptionPane.YES_OPTION) {
                    System.exit(0);
                } else {
                    frame3.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
                }
                break;
            case "carDetection":

                break;
            case "peopleDetection":
                break;
            case "faceDetection":
                break;
            case "plateDetection":
                break;
        }
    }

}
